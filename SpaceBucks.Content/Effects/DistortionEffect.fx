
sampler TextureSampler : register(s0);

// http://www.francois-tarlier.com/blog/cubic-lens-distortion-shader/
// Also simpler one here: http://xboxforums.create.msdn.com/forums/p/93304/560442.aspx#560442
float4 PixelShaderFunction(float2 tex: TEXCOORD0) : COLOR0
{
	float k = -0.15;
       
	// cubic distortion value
	float kcube = 0.5;
       
	float r2 = (tex.x-0.5) * (tex.x-0.5) + (tex.y-0.5) * (tex.y-0.5); 
	float f = 0;
       
 
	//only compute the cubic distortion if necessary
	if( kcube == 0.0)
	{
		f = 1 + r2 * k;
	}
	else
	{
		f = 1 + r2 * (k + kcube * sqrt(r2));
	}
       
	// get the right pixel for the current position
	float2 distortedTex = float2(f * (tex.x - 0.5)+0.5, f * (tex.y - 0.5) + 0.5);
	if(distortedTex.x < 0 || distortedTex.x > 1 || distortedTex.y < 0 || distortedTex.y > 1)
	{
		return float4(0, 0, 0, 1);
	}

	// Should this color shifting be done before distorting..? Maybe?
	// Also maybe moving color shifting could look more interesting
	static const float ColorShiftAmount = 0.001;
	float r = tex2D(TextureSampler, distortedTex).r;
	float g = tex2D(TextureSampler, distortedTex + ColorShiftAmount).g;
	float b = tex2D(TextureSampler, distortedTex - ColorShiftAmount).b;

	return float4(r, g, b, 1);
}


technique Technique1
{
    pass Pass1
	{
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
