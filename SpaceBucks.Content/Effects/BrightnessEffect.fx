
sampler TextureSampler : register(s0);
float4 PixelShaderFunction(float2 texCoordinate: TEXCOORD0) : COLOR0
{
    float4 color = tex2D(TextureSampler, texCoordinate);
	color.rgb /= color.a;
	color.rgb += 0.1 * color.rgb;
	color.rgb *= color.a;

	return color;
}

technique Technique1
{
    pass Pass1
	{
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}
