// Okay, fuck! This doesn't currently work properly on ellipses. The stroke is messed not "scaled" properly.. not sure how to fix it

// Color of the inner are in the circle
float4 FillColor = float4(0, 0, 0.25, 0.75);

/* STORKE */
float2 StrokeSize = 0.01; // Stroke size in TEXTURE COORDINATES! That means, if StrokeSize = 0.1, then the stroke will be 10% of the circle size
float4 StrokeColor = float4(1, 1, 1, 1);
bool SmoothStroke = false;

float4 PS(float2 texCoord: TEXCOORD0) : COLOR
{
	// trigonometry: if the point is outside the circle, then return transparent color
	float distanceFromCenter = sqrt(pow(abs(0.5 - texCoord.x), 2) + pow(abs(0.5 - texCoord.y), 2));
	if(distanceFromCenter > 0.5)
	{
		return float4(0, 0, 0, 0);
	}
	else if(distanceFromCenter > 0.5 - StrokeSize.x || distanceFromCenter > 0.5 - StrokeSize.y)
	{
		if(SmoothStroke)
		{
			float centerStroke = 0.5 - StrokeSize / 2;
			float alpha = pow(lerp(1, 0, abs(centerStroke - distanceFromCenter) / StrokeSize * 2), 2);

			if(distanceFromCenter < centerStroke)
			{
				return float4(StrokeColor.rgb * alpha + FillColor * (1-alpha), 1);
			}
			
			return float4(StrokeColor.rgb * alpha, alpha); 
		}
		else
		{
			return StrokeColor;
		}
	}
		
	return FillColor;
}

technique
{
	pass P0
	{
		PixelShader = compile ps_2_0 PS();
	}
}
