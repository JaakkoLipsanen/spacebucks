﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Flai;
using Flai.Graphics;
using Flai.Misc;
using Microsoft.Xna.Framework;
using SpaceBucks.Model;

namespace SpaceBucks.View
{
    public class WaveRenderer : FlaiRenderer
    {
        private readonly World _world;
        private readonly Dictionary<Enemy, EnemyVisualState> _enemyVisualStates = new Dictionary<Enemy, EnemyVisualState>();

        public WaveRenderer(World world)
        {
            _world = world;
        }

        protected override void LoadContentInner()
        {
            _world.WaveCompleted += (waveIndex, completionCoins) =>
            {
                _enemyVisualStates.Clear();
            };
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            foreach (EnemyVisualState enemyVisualState in _enemyVisualStates.Values)
            {
                enemyVisualState.Update(updateContext);
            }
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            foreach (Enemy enemy in _world.CurrentWave.Enemies)
            {
                EnemyVisualState visualState;
                if (!_enemyVisualStates.TryGetValue(enemy, out visualState))
                {
                    visualState = new EnemyVisualState(enemy);
                    _enemyVisualStates.Add(enemy, visualState);
                }

                this.DrawEnemyVisualState(graphicsContext, visualState);
                this.DrawEnemyHealthBar(graphicsContext, enemy);
            }
        }

        private void DrawEnemyVisualState(GraphicsContext graphicsContext, EnemyVisualState visualState)
        {
            foreach (var particle in visualState.Particles.Reverse())
            {
                int rgb = (int)FlaiMath.Scale(NoiseGenerator.GetNoise(particle.ID, particle.ID * 5.42f), 0, 1, 128, 255);
                graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, visualState.Enemy.Position + particle.Position, Color.Lerp(new Color(rgb, rgb, rgb), visualState.Enemy.Color, 0.5f) * particle.Alpha, 0, Tile.Size / 2f * particle.Scale);
            }
        }

        private void DrawEnemyHealthBar(GraphicsContext graphicsContext, Enemy enemy)
        {
            const int BorderSize = 2;
            const int Height = 4;
            const int Width = 40;

            float x = enemy.Position.X - Width / 2;
            float y = enemy.Position.Y - 30;


            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, new RectangleF(x - BorderSize, y - BorderSize, Width + BorderSize * 2, Height + BorderSize * 2), Color.DarkGray);
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, new RectangleF(x, y, Width, Height), Color.Red);
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, new RectangleF(x, y, (enemy.CurrentHealth / enemy.MaximumHealth) * Width, Height), new Color(0, 196, 0));
        }

        #region EnemyVisualState

        // Almost completely copied from DarkLight's Goal class
        private class EnemyVisualState
        {
            private const float DirectionStep = 1f;
            private const float Radius = EnemyRectangleParticle.Life * EnemyRectangleParticle.Speed;
            private const int ParticleCount = 20;
            private const float TimeBetweenRects = EnemyRectangleParticle.Life / EnemyVisualState.ParticleCount;

            private readonly Enemy _enemy;
            private readonly List<EnemyRectangleParticle> _particles;
            private readonly ReadOnlyCollection<EnemyRectangleParticle> _readOnlyParticles;

            private bool _hasUpdatedBefore = false;
            private float _timeSinceLastRect = 0f;
            private float _nextDirection = 0f;

            public Enemy Enemy
            {
                get { return _enemy; }
            }

            public ReadOnlyCollection<EnemyRectangleParticle> Particles
            {
                get { return _readOnlyParticles; }
            }

            public EnemyVisualState(Enemy enemy)
            {
                _enemy = enemy;

                _particles = new List<EnemyRectangleParticle>();
                _readOnlyParticles = new ReadOnlyCollection<EnemyRectangleParticle>(_particles);
            }

            public void Update(UpdateContext updateContext)
            {
                // If this is the first update, lets do bunch of fake updates!! Reason -> to make goal particles already spawned when the level starts
                if (!_hasUpdatedBefore && updateContext.DeltaSeconds > 0)
                {
                    _hasUpdatedBefore = true;
                    for (int i = 0; i < EnemyRectangleParticle.Life / updateContext.DeltaSeconds; i++)
                    {
                        this.Update(updateContext);
                    }
                }

                for (int i = 0; i < _particles.Count; i++)
                {
                    _particles[i].Update(updateContext);
                }
                _particles.RemoveAll(particle => particle.Alpha <= 0);

                _timeSinceLastRect += updateContext.DeltaSeconds;
                while (_timeSinceLastRect > EnemyVisualState.TimeBetweenRects)
                {
                    _timeSinceLastRect -= EnemyVisualState.TimeBetweenRects;
                    _particles.Add(new EnemyRectangleParticle(Vector2.Normalize(FlaiMath.GetAngleVector(_nextDirection))));
                    _nextDirection += EnemyVisualState.DirectionStep;
                }
            }

            #region Enemy Rectangle Particle

            public class EnemyRectangleParticle
            {
                public const float Life = 3f;
                public const float Speed = 10;

                private float _currentLife;
                private Vector2 _position;
                private Vector2 _direction;

                private static int NextID = 0;

                public readonly int ID = EnemyRectangleParticle.NextID++;

                public float Alpha
                {
                    get { return (EnemyRectangleParticle.Life - _currentLife) / EnemyRectangleParticle.Life; }
                }

                public float Scale
                {
                    get { return this.Alpha; }
                }

                public Vector2 Position
                {
                    get { return _position; }
                }

                public EnemyRectangleParticle(Vector2 direction)
                {
                    _position = Vector2.Zero;
                    _direction = direction;
                    _currentLife = 0f;
                }

                public void Update(UpdateContext updateContext)
                {
                    _currentLife += updateContext.DeltaSeconds;
                    _position += _direction * updateContext.DeltaSeconds * EnemyRectangleParticle.Speed;
                }
            }

            #endregion
        }

        #endregion
    }
}
