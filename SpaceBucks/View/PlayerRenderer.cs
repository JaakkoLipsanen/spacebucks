﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceBucks.Model;
using SpaceBucks.Model.Towers;

namespace SpaceBucks.View
{
    public class PlayerRenderer : FlaiRenderer
    {
        private readonly Player _player;
        private readonly World _world;

        private Texture2D _heartTexture;
        private Texture2D _coinTexture;

        public PlayerRenderer(Player player, World world)
        {
            _player = player;
            _world = world;
        }

        protected override void LoadContentInner()
        {
            _heartTexture = _contentProvider.DefaultManager.LoadTexture("Heart");
            _coinTexture = _contentProvider.DefaultManager.LoadTexture("Coin");
        }

        protected override void UnloadInner()
        {
            _serviceContainer.GetService<ICameraManager2D>().RemoveCamera("Player");
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            if (_player.CurrentlyAddingTower != null)
            {
                this.DrawFreePositionGrid(graphicsContext);
                this.DrawSelectedNewTower(graphicsContext);
            }

            this.DrawLivesAndCoins(graphicsContext);
        }

        private void DrawFreePositionGrid(GraphicsContext graphicsContext)
        {
            const int BorderSize = 2;
            for (int y = 0; y < _world.Map.Height; y++)
            {
                for (int x = 0; x < _world.Map.Width; x++)
                {
                    Color borderColor = _world.Map.IsFree(x, y) ? Color.LightGreen : Color.PaleVioletRed;
                    Color color = _world.Map.IsFree(x, y) ? Color.Green : Color.Red;
                    graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, 
                        new RectangleF(x * Tile.Size + BorderSize / 2f, y * Tile.Size + BorderSize / 2f, Tile.Size - BorderSize, Tile.Size - BorderSize),
                            borderColor * 0.25f, BorderSize); 

                    graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, 
                        new Rectangle(x * Tile.Size + BorderSize, y * Tile.Size + BorderSize, Tile.Size - BorderSize * 2, Tile.Size - BorderSize * 2),
                        color * 0.25f);
                }
            }
        }

        private void DrawSelectedNewTower(GraphicsContext graphicsContext)
        {
            Color backgroundColor = Color.White * 0.75f;
            Color foregroundColor = Color.Black;

            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, new Rectangle(_player.MouseIndex.X * Tile.Size, _player.MouseIndex.Y * Tile.Size, Tile.Size, Tile.Size), backgroundColor);
            graphicsContext.SpriteBatch.DrawStringCentered(graphicsContext.FontContainer.DefaultFont, _player.CurrentlyAddingTower.TowerType.ToChar(), new Vector2(_player.MouseIndex.X + 0.5f, _player.MouseIndex.Y + 0.5f) * Tile.Size, foregroundColor);
        }

        private void DrawLivesAndCoins(GraphicsContext graphicsContext)
        {
            // Lives
            graphicsContext.SpriteBatch.Draw(_heartTexture, new Vector2(6, graphicsContext.ScreenSize.Y - Tile.Size * 2 + 6), Color.White, 0, 3);
            graphicsContext.SpriteBatch.DrawString(graphicsContext.FontContainer["Minercraftory20"], _player.Lives.ToString(), new Vector2(6 + _heartTexture.Width * 3 + 6, graphicsContext.ScreenSize.Y - Tile.Size * 2), Color.White);

            // Space Bucks
            graphicsContext.SpriteBatch.Draw(_coinTexture, new Vector2(6, graphicsContext.ScreenSize.Y - 6 - _coinTexture.Height * 3), Color.White, 0, 3);
            graphicsContext.SpriteBatch.DrawString(graphicsContext.FontContainer["Minercraftory20"], _player.Coins.ToString(), new Vector2(6 + _coinTexture.Width * 3 + 6, graphicsContext.ScreenSize.Y - 12 - _coinTexture.Height * 3), Color.White);
        }
    }
}
