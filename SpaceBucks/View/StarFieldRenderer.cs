﻿using Flai;
using Flai.Graphics;
using System.Linq;
using Microsoft.Xna.Framework;
using SpaceBucks.Model;

namespace SpaceBucks.View
{
    public class StarFieldRenderer : FlaiRenderer
    {
        // Even though star field is at least in theory part of the model,
        // there is no reason why the actual object should belong to the world or
        // any other part of the model since it is only a graphical effect and has no effect
        // on anything other than its self.
        private StarField _starField;

        public StarFieldRenderer()
        {
        }

        protected override void LoadContentInner()
        {
            _starField = new StarField(new Vector2i(_graphicsDevice.Viewport.Width, _graphicsDevice.Viewport.Height));
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _starField.Update(updateContext);
        }

        // todo: allow zooming? it could be done so that the star-area is repeated
        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            foreach (Star star in _starField.Stars)
            {
                Color finalColor = Color.Lerp(Color.DimGray, Color.White, star.Brightness);
                graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, star.Position, finalColor, 0, star.Size);

                // Check if the star is partly off the screen (and thus needs to be drawn twice)
                float newX = float.MinValue;     
                if (star.Position.X > graphicsContext.ViewportSize.X - star.Size)
                {
                    newX = star.Position.X - graphicsContext.ViewportSize.X;
                }

                float newY = float.MinValue;
                if (star.Position.Y > graphicsContext.ViewportSize.Y - star.Size)
                {
                    newY = star.Position.Y - graphicsContext.ViewportSize.Y;
                }

                if (newX != float.MinValue || newY != float.MinValue)
                {
                    Vector2 newPosition = new Vector2(
                        newX == float.MinValue ? star.Position.X : newX, 
                        newY == float.MinValue ? star.Position.Y : newY);

                    graphicsContext.SpriteBatch.Draw(graphicsContext.BlankTexture, newPosition, finalColor, 0, star.Size);
                }   
            }
        }
    }
}
