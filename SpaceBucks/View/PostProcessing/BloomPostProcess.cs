﻿using System;
using Flai;
using Flai.Content;
using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceBucks.View.PostProcessing
{
    public class BloomPostProcess : PostProcess
    {
        #region Fields

        private float _bloomThreshold = 0.55f;
        private float _bloomBlurAmount = 4f;
        private float _bloomIntensity = 0.3f;
        private float _bloomBaseIntensity = 1f;
        private float _bloomSaturation = 3;
        private float _bloomBaseSaturation = 0.85f;

        private Effect _bloomExtractEffect;
        private Effect _bloomCombineEffect;
        private Effect _gaussianBlurEffect;

        private EffectParameter _bloomThresholdParameter;
        private EffectParameter _bloomIntensityParameter;
        private EffectParameter _bloomBaseIntensityParameter;
        private EffectParameter _bloomSaturationParameter;
        private EffectParameter _bloomBaseSaturationParameter;

        private EffectParameter _blurSampleWeightsParameter;
        private EffectParameter _blurSampleOffsetsParameter;

        // I dont necessarily need this many RenderTargets
        private RenderTarget2D _sceneRenderTarget;
        private RenderTarget2D _renderTarget1;
        private RenderTarget2D _renderTarget2;

        private float[] _horizontalWeightSamples;
        private Vector2[] _horizontalOffsetSamples;

        private float[] _verticalWeightSamples;
        private Vector2[] _verticalOffsetSamples;

        #endregion

        #region Properties

        public float BloomThreshold
        {
            get { return _bloomThreshold; }
            set
            {
                if (_bloomThreshold != value)
                {
                    _bloomThreshold = value;
                    if (_bloomThresholdParameter != null)
                    {
                        _bloomThresholdParameter.SetValue(_bloomThreshold);
                    }
                }
            }
        }

        public float BlurAmount
        {
            get { return _bloomBlurAmount; }
            set
            {
                if (_bloomBlurAmount != value)
                {
                    _bloomBlurAmount = value;
                }
            }
        }

        public float BloomIntensity
        {
            get { return _bloomIntensity; }
            set { _bloomIntensity = value; }
        }

        public float BaseIntensity
        {
            get { return _bloomBaseIntensity; }
            set
            {
                if (_bloomBaseIntensity != value)
                {
                    _bloomBaseIntensity = value;
                    if (_bloomBaseIntensityParameter != null)
                    {
                        _bloomBaseIntensityParameter.SetValue(_bloomBaseIntensity);
                    }
                }
            }
        }

        public float BloomSaturation
        {
            get { return _bloomSaturation; }
            set
            {
                if (_bloomSaturation != value)
                {
                    _bloomSaturation = value;
                    if (_bloomSaturationParameter != null)
                    {
                        _bloomSaturationParameter.SetValue(_bloomSaturation);
                    }
                }
            }
        }

        public float BaseSaturation
        {
            get { return _bloomBaseSaturation; }
            set
            {
                if (_bloomBaseSaturation != value)
                {
                    _bloomBaseSaturation = value;
                    if (_bloomBaseSaturationParameter != null)
                    {
                        _bloomBaseSaturationParameter.SetValue(_bloomBaseSaturation);
                    }
                }
            }
        }

        #endregion

        public override void LoadContent(FlaiServiceContainer serviceContainer)
        {
            FlaiContentManager contentManager = serviceContainer.GetService<IContentProvider>().DefaultManager;
            _bloomExtractEffect = contentManager.LoadEffect("Bloom\\BloomExtract");
            _bloomCombineEffect = contentManager.LoadEffect("Bloom\\BloomCombine");
            _gaussianBlurEffect = contentManager.LoadEffect("Bloom\\GaussianBlur");

            _bloomThresholdParameter = _bloomExtractEffect.Parameters["BloomThreshold"];
            _bloomIntensityParameter = _bloomCombineEffect.Parameters["BloomIntensity"];
            _bloomBaseIntensityParameter = _bloomCombineEffect.Parameters["BaseIntensity"];
            _bloomSaturationParameter = _bloomCombineEffect.Parameters["BloomSaturation"];
            _bloomBaseSaturationParameter = _bloomCombineEffect.Parameters["BaseSaturation"];

            _blurSampleOffsetsParameter = _gaussianBlurEffect.Parameters["SampleOffsets"];
            _blurSampleWeightsParameter = _gaussianBlurEffect.Parameters["SampleWeights"];

            _bloomThresholdParameter.SetValue(_bloomThreshold);
            _bloomIntensityParameter.SetValue(_bloomIntensity);
            _bloomBaseIntensityParameter.SetValue(_bloomBaseIntensity);
            _bloomSaturationParameter.SetValue(_bloomSaturation);
            _bloomBaseSaturationParameter.SetValue(_bloomBaseSaturation);

            int sampleCount = _blurSampleWeightsParameter.Elements.Count;
            _horizontalWeightSamples = new float[sampleCount];
            _verticalWeightSamples = new float[sampleCount];
            _horizontalOffsetSamples = new Vector2[sampleCount];
            _verticalOffsetSamples = new Vector2[sampleCount];

            this.CreateRenderTargets();
            this.CalculateBlurSamples(1f / _renderTarget1.Width, 1f / _renderTarget1.Height);
            _graphicsDevice.DeviceReset += (o, e) =>
            {
                this.CreateRenderTargets();
                this.CalculateBlurSamples(1f / _renderTarget1.Width, 1f / _renderTarget1.Height);
            };
        }

        public override void Apply(GraphicsContext graphicsContext, RenderTarget2D input, RenderTarget2D output)
        {
            _graphicsDevice.SetRenderTarget(_sceneRenderTarget);

            graphicsContext.SpriteBatch.Begin();
            graphicsContext.SpriteBatch.DrawFullscreen(input);
            graphicsContext.SpriteBatch.End();

            this.InnerApply(graphicsContext, output);
        }

        private void InnerApply(GraphicsContext graphicsContext, RenderTarget2D output)
        {
            _graphicsDevice.SamplerStates[1] = SamplerState.LinearClamp;

            // Pass 1: draw the scene into rendertarget 1, using a
            // shader that extracts only the brightest parts of the image.
            this.DrawFullscreenQuad(graphicsContext, _sceneRenderTarget, _renderTarget1, _bloomExtractEffect);

            // Pass 2: draw from rendertarget 1 into rendertarget 2,
            // using a shader to apply a horizontal gaussian blur filter.
            this.SetHorizontalBlurSamples();
            this.DrawFullscreenQuad(graphicsContext, _renderTarget1, _renderTarget2, _gaussianBlurEffect);

            // Pass 3: draw from rendertarget 2 back into rendertarget 1,
            // using a shader to apply a vertical gaussian blur filter.
            this.SetVerticalBlurSamples();
            this.DrawFullscreenQuad(graphicsContext, _renderTarget2, _renderTarget1, _gaussianBlurEffect);

            // Pass 4: draw both rendertarget 1 and the original scene
            // image back into the main backbuffer, using a shader that
            // combines them to produce the final bloomed result.
            _graphicsDevice.SetRenderTarget(output);
            _graphicsDevice.Textures[1] = _sceneRenderTarget;

            this.DrawFullscreenQuad(
                graphicsContext, _renderTarget1,
                graphicsContext.ScreenSize.X, graphicsContext.ScreenSize.Y,
                _bloomCombineEffect);
        }

        private void CreateRenderTargets()
        {
            PresentationParameters pp = _graphicsDevice.PresentationParameters;
            if (_sceneRenderTarget != null)
            {
                _sceneRenderTarget.Dispose();
                _sceneRenderTarget = null;
            }
            _sceneRenderTarget = new RenderTarget2D(_graphicsDevice, pp.BackBufferWidth, pp.BackBufferHeight, false, pp.BackBufferFormat, pp.DepthStencilFormat, pp.MultiSampleCount, RenderTargetUsage.DiscardContents);

            if (_renderTarget1 != null)
            {
                _renderTarget1.Dispose();
                _renderTarget1 = null;
            }
            _renderTarget1 = new RenderTarget2D(_graphicsDevice, pp.BackBufferWidth / 2, pp.BackBufferHeight / 2, false, pp.BackBufferFormat, DepthFormat.None);

            if (_renderTarget2 != null)
            {
                _renderTarget2.Dispose();
                _renderTarget2 = null;
            }
            _renderTarget2 = new RenderTarget2D(_graphicsDevice, pp.BackBufferWidth / 2, pp.BackBufferHeight / 2, false, pp.BackBufferFormat, DepthFormat.None);
        }

        private void SetHorizontalBlurSamples()
        {
            _blurSampleWeightsParameter.SetValue(_horizontalWeightSamples);
            _blurSampleOffsetsParameter.SetValue(_horizontalOffsetSamples);
        }

        private void SetVerticalBlurSamples()
        {
            _blurSampleWeightsParameter.SetValue(_verticalWeightSamples);
            _blurSampleOffsetsParameter.SetValue(_verticalOffsetSamples);
        }

        /// <summary>
        /// Helper for drawing a texture into a rendertarget, using
        /// a custom shader to apply postprocessing effects.
        /// </summary>
        private void DrawFullscreenQuad(GraphicsContext graphicsContext, Texture2D texture, RenderTarget2D renderTarget, Effect effect)
        {
            _graphicsDevice.SetRenderTarget(renderTarget);
            this.DrawFullscreenQuad(graphicsContext, texture, renderTarget.Width, renderTarget.Height, effect);
        }

        /// <summary>
        /// Helper for drawing a texture into the current rendertarget,
        /// using a custom shader to apply postprocessing effects.
        /// </summary>
        private void DrawFullscreenQuad(GraphicsContext graphicsContext, Texture2D texture, int width, int height, Effect effect)
        {
            graphicsContext.SpriteBatch.Begin(0, BlendState.Opaque, null, null, null, effect);
            graphicsContext.SpriteBatch.Draw(texture, new Rectangle(0, 0, width, height), Color.White);
            graphicsContext.SpriteBatch.End();
        }

        /// <summary>
        /// Computes sample weightings and texture coordinate offsets
        /// for one pass of a separable gaussian blur filter.
        /// </summary>
        private void CalculateBlurSamples(float pixelWidth, float pixelHeight)
        {
            // The first sample always has a zero offset.
            _horizontalWeightSamples[0] = this.ComputeGaussian(0);
            _verticalWeightSamples[0] = this.ComputeGaussian(0);

            _horizontalOffsetSamples[0] = Vector2.Zero;
            _verticalOffsetSamples[0] = Vector2.Zero;

            // Maintain a sum of all the weighting values.
            Vector2 totalWeights = new Vector2(_horizontalWeightSamples[0], _verticalWeightSamples[0]);

            int sampleCount = _blurSampleWeightsParameter.Elements.Count;
            // Add pairs of additional sample taps, positioned
            // along a line in both directions from the center.
            for (int i = 0; i < sampleCount / 2; i++)
            {
                // Store weights for the positive and negative taps.
                float weight = this.ComputeGaussian(i + 1);
                _horizontalWeightSamples[i * 2 + 1] = weight;
                _horizontalWeightSamples[i * 2 + 2] = weight;
                _verticalWeightSamples[i * 2 + 1] = weight;
                _verticalWeightSamples[i * 2 + 2] = weight;

                totalWeights += Vector2.One * weight * 2;

                // To get the maximum amount of blurring from a limited number of
                // pixel shader samples, we take advantage of the bilinear filtering
                // hardware inside the texture fetch unit. If we position our texture
                // coordinates exactly halfway between two texels, the filtering unit
                // will average them for us, giving two samples for the price of one.
                // This allows us to step in units of two texels per sample, rather
                // than just one at a time. The 1.5 offset kicks things off by
                // positioning us nicely in between two texels.
                float sampleOffset = i * 2 + 1.5f;

                Vector2 horizontalSample = Vector2.UnitX * pixelWidth * sampleOffset;
                // Store texture coordinate offsets for the positive and negative taps.
                _horizontalOffsetSamples[i * 2 + 1] = horizontalSample;
                _horizontalOffsetSamples[i * 2 + 2] = -horizontalSample;

                Vector2 verticalSample = Vector2.UnitY * pixelHeight * sampleOffset;
                _verticalOffsetSamples[i * 2 + 1] = verticalSample;
                _verticalOffsetSamples[i * 2 + 2] = -verticalSample;
            }

            // Normalize the list of sample weightings, so they will always sum to one.
            for (int i = 0; i < sampleCount; i++)
            {
                _horizontalWeightSamples[i] /= totalWeights.X;
                _verticalWeightSamples[i] /= totalWeights.Y;
            }
        }

        /// <summary>
        /// Evaluates a single point on the gaussian falloff curve.
        /// Used for setting up the blur filter weightings.
        /// </summary>
        private float ComputeGaussian(float n)
        {
            float theta = _bloomBlurAmount;
            return (float)((1.0 / FlaiMath.Sqrt(2 * FlaiMath.Pi * theta)) * FlaiMath.Exp(-(n * n) / (2 * theta * theta)));
        }
    }
}
