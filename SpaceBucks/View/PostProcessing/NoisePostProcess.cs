﻿using Flai;
using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceBucks.View.PostProcessing
{
    public class NoisePostProcess : PostProcess
    {
        public override void Apply(GraphicsContext graphicsContext, RenderTarget2D input, RenderTarget2D output)
        {
            graphicsContext.GraphicsDevice.SetRenderTarget(output);
            graphicsContext.GraphicsDevice.Clear(Color.Black);

            graphicsContext.SpriteBatch.Begin();
            graphicsContext.SpriteBatch.DrawFullscreen(input);
            graphicsContext.SpriteBatch.End();

            graphicsContext.SpriteBatch.Begin(BlendState.Additive, SamplerState.PointWrap);
            Texture2D noiseTexture = graphicsContext.ContentProvider.DefaultManager.LoadTexture("Noise");
            graphicsContext.SpriteBatch.Draw(noiseTexture, graphicsContext.ScreenArea, new Rectangle(Global.Random.Next(-1000, 1000), Global.Random.Next(-1000, 1000), graphicsContext.ScreenSize.X, graphicsContext.ScreenSize.Y), Color.White * 0.3F);
            graphicsContext.SpriteBatch.End();

            graphicsContext.GraphicsDevice.SetRenderTarget(null);
        }
    }
}
