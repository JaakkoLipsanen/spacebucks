﻿using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceBucks.View.PostProcessing
{
    public class ScanLinePostProcess : PostProcess
    {
        public ScanLinePostProcess()
        {
        }

        public override void Apply(GraphicsContext graphicsContext, RenderTarget2D input, RenderTarget2D output)
        {
            BlendState blendState = new BlendState()
            {
                AlphaSourceBlend = Blend.SourceAlpha,
                AlphaDestinationBlend = Blend.DestinationAlpha,
                ColorSourceBlend = Blend.SourceAlpha,
                ColorDestinationBlend = Blend.DestinationAlpha,
                AlphaBlendFunction = BlendFunction.ReverseSubtract,
                ColorBlendFunction = BlendFunction.ReverseSubtract,
            };

            BlendState alpha = BlendState.AlphaBlend;
            BlendState add = BlendState.Additive;
            graphicsContext.GraphicsDevice.SetRenderTarget(output);
            graphicsContext.GraphicsDevice.Clear(Color.Black);

            graphicsContext.SpriteBatch.Begin();
            graphicsContext.SpriteBatch.DrawFullscreen(input);
            graphicsContext.SpriteBatch.End();

            graphicsContext.SpriteBatch.Begin(blendState, SamplerState.PointWrap);
            Texture2D scanLineTexture = graphicsContext.ContentProvider.DefaultManager.LoadTexture("ScanLine1x2");
            graphicsContext.SpriteBatch.Draw(scanLineTexture, Vector2.Zero, graphicsContext.ScreenArea, new Color(128, 128, 128, 128) * 0.95f, 0, Vector2.Zero, 1, SpriteEffects.None, 0F);
            graphicsContext.SpriteBatch.End();

            graphicsContext.GraphicsDevice.SetRenderTarget(null);
        }
    }
}
