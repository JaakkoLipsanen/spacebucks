﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceBucks.View.PostProcessing
{
    public class DistortionPostProcess  : PostProcess
    {
        public override void Apply(GraphicsContext graphicsContext, RenderTarget2D input, RenderTarget2D output)
        {
            graphicsContext.GraphicsDevice.SetRenderTarget(output);
            graphicsContext.GraphicsDevice.Clear(Color.Black);

            graphicsContext.SpriteBatch.Begin(graphicsContext.ContentProvider.DefaultManager.LoadEffect("DistortionEffect"));
            graphicsContext.SpriteBatch.DrawFullscreen(input);
            graphicsContext.SpriteBatch.End();

            graphicsContext.GraphicsDevice.SetRenderTarget(null);
        }
    }
}
