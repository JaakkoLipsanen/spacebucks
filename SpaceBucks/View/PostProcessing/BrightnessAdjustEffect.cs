﻿using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceBucks.View.PostProcessing
{
    public class BrightnessAdjustEffect : PostProcess
    {
        public override void Apply(GraphicsContext graphicsContext, RenderTarget2D input, RenderTarget2D output)
        {
            graphicsContext.GraphicsDevice.SetRenderTarget(output);
            graphicsContext.GraphicsDevice.Clear(Color.Black);

            graphicsContext.SpriteBatch.Begin(graphicsContext.ContentProvider.DefaultManager.LoadEffect("BrightnessEffect"));
            graphicsContext.SpriteBatch.DrawFullscreen(input);
            graphicsContext.SpriteBatch.End();

            graphicsContext.GraphicsDevice.SetRenderTarget(null);
        }
    }
}
