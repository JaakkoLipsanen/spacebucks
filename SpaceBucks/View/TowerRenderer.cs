﻿using System.Linq;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceBucks.Model;
using SpaceBucks.Model.Towers;
using SpaceBucks.Model.Towers.Bullets;
using SpaceBucks.Model.Towers.TowerProxys;

namespace SpaceBucks.View
{
    public class TowerRenderer : FlaiRenderer
    {
        private readonly WorldMap _worldMap;
        private readonly CircleRenderer _circleRenderer;

        public TowerRenderer(WorldMap worldMap)
        {
            _worldMap = worldMap;
            _circleRenderer = new CircleRenderer();
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            // Draw tower base
            foreach (TowerProxy towerProxy in _worldMap.TowerProxys)
            {
                Tower tower = towerProxy.Tower;
                TowerRenderer.DrawTowerBase(graphicsContext, tower.WorldIndex * Tile.Size, towerProxy.TowerType);

                // Draw tower borders
                if (tower.IsSelected)
                {
                    const int BorderThickness = 4;
                    graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(
                        graphicsContext,
                        new Rectangle(
                            tower.WorldIndex.X * Tile.Size + BorderThickness / 2, tower.WorldIndex.Y * Tile.Size + BorderThickness / 2,
                            Tile.Size - BorderThickness, Tile.Size - BorderThickness),
                        Color.LawnGreen, BorderThickness);
                }
            }

            // Draw bullets
            foreach (Tower tower in _worldMap.Towers.Where(tower => tower is RegularTower))
            {
                // Draw bullets.  TODO: This is really really cumberstone and not a good way! Maybe the bullets could be removed from the Tower itself and be tracked in World for example..?
                foreach (Bullet bullet in (tower as RegularTower).Bullets)
                {
                    bullet.Draw(graphicsContext);
                }
            }


            // Draw additive bullets // OKAY THIS IS A HORRIBLE HACK ETC I HATE IT ETC but i just have to live with it I guess..
            graphicsContext.SpriteBatch.End();
            graphicsContext.SpriteBatch.Begin(BlendState.Additive, SamplerState.PointClamp);

            foreach (Tower tower in _worldMap.Towers.Where(tower => tower is RegularTower))
            {
                // Draw bullets.  TODO: This is really really cumberstone and not a good way! Maybe the bullets could be removed from the Tower itself and be tracked in World for example..?
                foreach (Bullet bullet in (tower as RegularTower).Bullets)
                {
                    bullet.DrawAdditive(graphicsContext);
                }
            }

            graphicsContext.SpriteBatch.End();
            graphicsContext.SpriteBatch.Begin(SamplerState.PointClamp);
        }

        public void DrawOther(GraphicsContext graphicsContext)
        {
            // Should this be at PlayerRenderer...? After all, the tower IS selected by the player! 
            // And it's a bit messy if Tower actually knows if it's selected, there is no actual need for it to know it....
            RegularTower selectedTower = _worldMap.Towers.FirstOrDefault(tower => tower.IsSelected && tower is RegularTower) as RegularTower;
            if (selectedTower != null)
            {
                _circleRenderer.Begin(graphicsContext);
                _circleRenderer.DrawCircle(graphicsContext, selectedTower.Area.Center, Color.Blue * 0.15f, selectedTower.AttackRange, Color.White * 0.75f, 4);
                _circleRenderer.End(graphicsContext);
            }
        }

        public static void DrawTowerBase(GraphicsContext graphicsContext, Vector2 position, TowerType towerType)
        {
            TowerRenderer.DrawTowerBase(graphicsContext, position, towerType, Color.White, Color.Black);
        }

        public static void DrawTowerBase(GraphicsContext graphicsContext, Vector2 position, TowerType towerType, Color backgroundColor, Color foregroundColor)
        {
            graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, new RectangleF(position.X, position.Y, Tile.Size, Tile.Size), backgroundColor);
            graphicsContext.SpriteBatch.DrawStringCentered(graphicsContext.FontContainer.DefaultFont, towerType.ToChar(), position + Vector2.One * 0.5f * Tile.Size, foregroundColor);
        }
    }
}
