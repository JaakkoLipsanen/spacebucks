﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceBucks.Model;

namespace SpaceBucks.View
{
    public class WorldMapRenderer : FlaiRenderer
    {
        private static readonly Color BorderColor = Color.White * 0.8f;

        private static readonly Color GoodSolidColor1 = new Color(55, 160, 95) * 0.35f;
        private static readonly Color GoodSolidColor2 = Color.SpringGreen * 0.25f;
        private static readonly Color GoodSolidColor3 = Color.DodgerBlue * 0.25f;
        private static readonly Color GoodSolidColor4 = Color.PaleVioletRed * 0.5f;
        private static readonly Color GoodSolidColor5 = Color.IndianRed * 0.4f;
        private static readonly Color GoodSolidColor6 = Color.CadetBlue * 0.4f;

        private readonly WorldMap _worldMap;
        private readonly TowerRenderer _towerRenderer;

        private float _totalTime = 0f;

        public WorldMapRenderer(WorldMap worldMap)
        {
            _worldMap = worldMap;
            _towerRenderer = new TowerRenderer(_worldMap);
        }

        protected override void LoadContentInner()
        {
            _towerRenderer.LoadContent();
        }

        protected override void UnloadInner()
        {
            _towerRenderer.Unload();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _totalTime += updateContext.DeltaSeconds;
            _towerRenderer.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            this.DrawSolidTiles(graphicsContext);
            this.DrawTileBorders(graphicsContext);

            _towerRenderer.Draw(graphicsContext);
        }

        public void DrawOther(GraphicsContext graphicsContext)
        {
            _towerRenderer.DrawOther(graphicsContext);
        }

        private void DrawSolidTiles(GraphicsContext graphicsContext)
        {
            // AARGH!! This is ridicoulous, any slower than 0.4f will make the animation very janky...
            // I think it could be possible to solve this by rendering via 3D API but... argh
            // EDIT: It's not that bad in the smooth version if the texture moves only in x-axis
            const float Speed = 0.4f;

            Texture2D texture = graphicsContext.ContentProvider.DefaultManager.LoadTexture("FullSquare");
            for (int x = -8; x < _worldMap.Width + 8; x++)
            {
                for (int y = 0; y < _worldMap.Height; y++)
                {
                    if (_worldMap.IsSolid(x, y))
                    {
                        // graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, center, Color.Black * 0.5f, 0f, Tile.Size);
                        float sourceRectX = _totalTime * Speed * texture.Width;
                        float sourceRectY = _totalTime * Speed * texture.Height;
                        graphicsContext.SpriteBatch.Draw(
                            texture, new RectangleF(x * Tile.Size, y * Tile.Size, Tile.Size, Tile.Size),
                            new Rectangle((int)sourceRectX, 0, texture.Width, texture.Height),
                            GoodSolidColor6);
                    }
                }
            }
        }

        // TODO: Make special cases for corners
        private void DrawTileBorders(GraphicsContext graphicsContext)
        {
            const float LineThickness = 6f;
            for (int x = 0; x < _worldMap.Width; x++)
            {
                for (int y = 0; y < _worldMap.Height; y++)
                {
                    if (_worldMap.IsSolid(x, y))
                    {
                        bool isLeftFree = x != 0 && !_worldMap.IsSolid(x - 1, y);
                        bool isRightFree = x != _worldMap.Width - 1 && !_worldMap.IsSolid(x + 1, y);
                        bool isTopFree = y != 0 && !_worldMap.IsSolid(x, y - 1);
                        bool isBottomFree = y != _worldMap.Height - 1 && !_worldMap.IsSolid(x, y + 1);

                        // Left
                        if (isLeftFree)
                        {
                            graphicsContext.PrimitiveRenderer.DrawLine(
                                graphicsContext,
                                new Vector2(x * Tile.Size - LineThickness / 2f, y * Tile.Size),
                                new Vector2(x * Tile.Size - LineThickness / 2f, (y + 1) * Tile.Size),
                                WorldMapRenderer.BorderColor, LineThickness);
                        }

                        // Right
                        if (isRightFree)
                        {
                            graphicsContext.PrimitiveRenderer.DrawLine(
                                graphicsContext,
                                new Vector2((x + 1) * Tile.Size + LineThickness / 2f, y * Tile.Size),
                                new Vector2((x + 1) * Tile.Size + LineThickness / 2f, (y + 1) * Tile.Size),
                                WorldMapRenderer.BorderColor, LineThickness);
                        }

                        // Top
                        if (isTopFree)
                        {
                            graphicsContext.PrimitiveRenderer.DrawLine(
                                graphicsContext,
                                new Vector2(x * Tile.Size, y * Tile.Size - LineThickness / 2f),
                                new Vector2((x + 1) * Tile.Size, y * Tile.Size - LineThickness / 2f),
                                WorldMapRenderer.BorderColor, LineThickness);
                        }

                        // Bottom
                        if (isBottomFree)
                        {
                            graphicsContext.PrimitiveRenderer.DrawLine(
                                graphicsContext,
                                new Vector2(x * Tile.Size, (y + 1) * Tile.Size + LineThickness / 2f),
                                new Vector2((x + 1) * Tile.Size, (y + 1) * Tile.Size + LineThickness / 2f),
                                WorldMapRenderer.BorderColor, LineThickness);
                        }
                    }
                }
            }
        }
    }
}
