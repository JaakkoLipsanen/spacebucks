﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using SpaceBucks.Model;

namespace SpaceBucks.View
{
    public class WorldRenderer : FlaiRenderer
    {
        private readonly World _world;
        private readonly WorldMapRenderer _worldMapRenderer;
        private readonly WaveRenderer _waveRenderer;
        private readonly StarFieldRenderer _starFieldRenderer;

        public WorldRenderer(World world)
        {
            _world = world;

            _worldMapRenderer = new WorldMapRenderer(_world.Map);
            _waveRenderer = new WaveRenderer(_world);
            _starFieldRenderer = new StarFieldRenderer();
        }

        protected override void LoadContentInner()
        {
            _starFieldRenderer.LoadContent();
            _worldMapRenderer.LoadContent();
            _waveRenderer.LoadContent();
        }

        protected override void UnloadInner()
        {
            _starFieldRenderer.Unload();
            _worldMapRenderer.Unload();
            _waveRenderer.Unload();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _starFieldRenderer.Update(updateContext);
            _worldMapRenderer.Update(updateContext);
            _waveRenderer.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            _starFieldRenderer.Draw(graphicsContext);
            _worldMapRenderer.Draw(graphicsContext);
            _waveRenderer.Draw(graphicsContext);
        }

        public void DrawOther(GraphicsContext graphicsContext)
        {
            _worldMapRenderer.DrawOther(graphicsContext);
        }
    }
}