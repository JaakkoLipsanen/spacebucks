﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceBucks.View
{
    public class CursorRenderer : FlaiRenderer
    {
        private Vector2i _mousePosition;
        private Texture2D _cursorTexture;

        protected override void LoadContentInner()
        {
            _cursorTexture = _contentProvider.DefaultManager.LoadTexture("Cursor");
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _mousePosition = updateContext.InputState.MousePosition;
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.Begin();
            graphicsContext.SpriteBatch.Draw(_cursorTexture, _mousePosition);
            graphicsContext.SpriteBatch.End();
        }
    }
}
