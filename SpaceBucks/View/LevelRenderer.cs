﻿using Flai;
using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SpaceBucks.Model;
using SpaceBucks.View.PostProcessing;

namespace SpaceBucks.View
{
    public class LevelRenderer : FlaiRenderer
    {
        private static readonly Color BackgroundColor = new Color(16, 16, 44);

        private readonly Level _level;
        private readonly WorldRenderer _worldRenderer;
        private readonly PlayerRenderer _playerRenderer;

        public LevelRenderer(Level level)
        {
            _level = level;
            _worldRenderer = new WorldRenderer(_level.World);
            _playerRenderer = new PlayerRenderer(_level.Player, _level.World);
        }

        protected override void LoadContentInner()
        {
            _worldRenderer.LoadContent();
            _playerRenderer.LoadContent();
        }

        protected override void UnloadInner()
        {
            _playerRenderer.Unload();
            _worldRenderer.Unload();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _playerRenderer.Update(updateContext);
            _worldRenderer.Update(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            graphicsContext.GraphicsDevice.Clear(LevelRenderer.BackgroundColor);

            // Draw foreground (with camera (if there was one))
            graphicsContext.SpriteBatch.Begin(SamplerState.PointWrap);
            _worldRenderer.Draw(graphicsContext);
            _playerRenderer.Draw(graphicsContext);
            graphicsContext.SpriteBatch.End();

            // Draw stuff that can't be inside the main draw loop
            _worldRenderer.DrawOther(graphicsContext);
        }
    }
}
