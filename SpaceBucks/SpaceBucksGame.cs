using System;
using Flai;
using Flai.Graphics;
using Flai.Graphics.PostProcessing;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using SpaceBucks.Screens;
using SpaceBucks.View;
using SpaceBucks.View.PostProcessing;

namespace SpaceBucks
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class SpaceBucksGame : FlaiGame
    {
        private readonly PostProcessRenderer _postProcessRenderer;
        private readonly CursorRenderer _cursorRenderer;

        public SpaceBucksGame()
        {
            base.WindowTitle = "Space Bucks";
            base.ClearColor = Color.Black;
            base.InactiveSleepTime = TimeSpan.Zero;

            Cursor.IsVisible = false;
            _contentProvider.RootDirectory = "SpaceBucks.Content";

            _postProcessRenderer = new PostProcessRenderer(4, null);
            _cursorRenderer = new CursorRenderer();
        }

        protected override void LoadContentInner()
        {
            _cursorRenderer.LoadContent();
            _postProcessRenderer.LoadContent();

            _postProcessRenderer.AddPostProcess(new BloomPostProcess());
            _postProcessRenderer.AddPostProcess(new BrightnessAdjustEffect());
            _postProcessRenderer.AddPostProcess(new ScanLinePostProcess());
            _postProcessRenderer.AddPostProcess(new NoisePostProcess());
            _postProcessRenderer.AddPostProcess(new DistortionPostProcess());
        }

        protected override void UnloadContentInner()
        {
            _cursorRenderer.Unload();
            _postProcessRenderer.Unload();
        }

        protected override void UpdateInner(UpdateContext updateContext)
        {
            _screenManager.Update(updateContext);
            _cursorRenderer.Update(updateContext);

            this.CheckForTogglePostProcessesInput(updateContext);
        }

        protected override void DrawInner(GraphicsContext graphicsContext)
        {
            // Pre-draw all screens
            _screenManager.PreDraw(graphicsContext);

            // Draw the screens
            _postProcessRenderer.BeginDraw(graphicsContext);
            _screenManager.Draw(graphicsContext);

            // Draw the cursor
            _cursorRenderer.Draw(graphicsContext);

            // Render the final image with post-processes applied
            _postProcessRenderer.Draw(graphicsContext);
        }

        protected override void AddInitialScreens()
        {
            _screenManager.AddScreen(new GameplayScreen());
        }

        protected override void InitializeGraphicsSettings()
        {
            _graphicsDeviceManager.PreferredBackBufferWidth = 1280;
            _graphicsDeviceManager.PreferredBackBufferHeight = 960;
            _graphicsDeviceManager.PreferMultiSampling = true;
            _graphicsDeviceManager.SynchronizeWithVerticalRetrace = false;
        }

        private void CheckForTogglePostProcessesInput(UpdateContext updateContext)
        {
            if (updateContext.InputState.IsNewKeyPress(Keys.F1))
            {
                _postProcessRenderer.GetPostProcess<BloomPostProcess>().ToggleEnabled();
            }
            else if (updateContext.InputState.IsNewKeyPress(Keys.F2))
            {
                _postProcessRenderer.GetPostProcess<BrightnessAdjustEffect>().ToggleEnabled();
            }
            else if (updateContext.InputState.IsNewKeyPress(Keys.F3))
            {
                _postProcessRenderer.GetPostProcess<ScanLinePostProcess>().ToggleEnabled();
            }
            else if (updateContext.InputState.IsNewKeyPress(Keys.F4))
            {
                _postProcessRenderer.GetPostProcess<NoisePostProcess>().ToggleEnabled();
            }
            else if (updateContext.InputState.IsNewKeyPress(Keys.F5))
            {
                _postProcessRenderer.GetPostProcess<DistortionPostProcess>().ToggleEnabled();
            }
        }
    }

    #region Entry Point

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (SpaceBucksGame game = new SpaceBucksGame())
            {
                game.Run();
            }
        }
    }

    #endregion
}
