﻿using Flai;
using Flai.Input;
using Microsoft.Xna.Framework.Input;
using SpaceBucks.Model;
using SpaceBucks.Model.Towers.TowerProxys;

namespace SpaceBucks.Controller
{
    // TODO: If adding a tower and shift is pressed down, then if the user
    // presses the left mouse button down, towers should be added "constantly"
    public class PlayerController : FlaiController
    {
        private readonly Player _player;
        private readonly World _world;

        public PlayerController(World world, Player player)
        {
            _world = world;
            _player = player;
        }

        public override void Control(UpdateContext updateContext)
        {
            _player.MouseIndex = new Vector2i(updateContext.InputState.MousePosition / Tile.Size);
            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Right))
            {
                _player.CurrentlyAddingTower = null;
                _player.SelectedTower = null;
            }

            if (updateContext.InputState.IsNewMouseButtonPress(MouseButton.Left))
            {
                if (_player.CurrentlyAddingTower != null)
                {
                    this.HandleAddTowerLogic(updateContext);
                }
                else
                {
                    this.HandleSelectTowerLogic(updateContext);
                }
            }
        }

        private void HandleAddTowerLogic(UpdateContext updateContext)
        {
            // Try to add a tower to the world
            TowerProxy towerProxy = _player.CurrentlyAddingTower.Clone();

            // If the tower adding was succesful and LeftShift is not pressed down, then set the CurrentlyAddingTower to null
            if(!_player.BuyTower(towerProxy, _player.MouseIndex) || !updateContext.InputState.IsKeyPressed(Keys.LeftShift) || _player.Coins < _player.CurrentlyAddingTower.Cost)
            {
                _player.CurrentlyAddingTower = null;
            }
        }

        private void HandleSelectTowerLogic(UpdateContext updateContext)
        {
            if (_world.Map.IsSolid(_player.MouseIndex) && _world.Map.IsBuildable(_player.MouseIndex))
            {
                _player.SelectedTower = _world.Map.GetTowerProxyAt(_player.MouseIndex);
            }
        }
    }
}
