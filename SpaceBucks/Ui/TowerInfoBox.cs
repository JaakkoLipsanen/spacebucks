﻿using Flai;
using Flai.Extensions;
using Flai.Graphics;
using Flai.Ui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceBucks.Model;
using SpaceBucks.Model.Towers.TowerProxys;
using SpaceBucks.View;

namespace SpaceBucks.Ui
{
    public class TowerInfoBox : UiObject
    {
        private readonly Player _player;
        private readonly BasicUiContainer _basicUiContainer = new BasicUiContainer();
        private TowerUpgradeList _currentTowerUpgradeList;

        public TowerInfoBox(Player player)
        {
            _player = player;
            _player.SelectedTowerChanged += OnPlayerSelectedTowerChanged;
        }

        public override void Update(UpdateContext updateContext)
        {
            _basicUiContainer.Update(updateContext);
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            _basicUiContainer.Draw(graphicsContext, true);
        }

        private void OnPlayerSelectedTowerChanged(TowerProxy newTowerProxy)
        {
            if (_currentTowerUpgradeList != null)
            {
                _currentTowerUpgradeList.OnRemoved();
            }

            // Remove old UI elements
            _basicUiContainer.Clear();
            if (newTowerProxy == null)
            {
                return;
            }

            this.CreateUiElements(newTowerProxy); 
        }

        #region CreateUiElements

        private void CreateUiElements(TowerProxy newTowerProxy)
        {
            Vector2i screenSize = FlaiGame.Current.WindowSize;
            const int PaddingFromTop = 4;
            const int HorizontalPadding = 8;
            const string FontName = "Minercraftory20";
            SpriteFont font = FlaiGame.Current.FontContainer[FontName];
            float fontHeight = font.GetCharacterHeight();

            // Name
            string towerName = _player.SelectedTower.Name;
            float towerNameWidth = font.GetStringWidth(towerName);

            // Add the new UI elements

            // Tower name
            TextBlock towerNameTextBlock = new TextBlock(newTowerProxy.Name, new Vector2(screenSize.X - towerNameWidth - HorizontalPadding, PaddingFromTop)) { Font = FontName, IsCentered = false };
            _basicUiContainer.Add(towerNameTextBlock);

            // Upgrade list
            _currentTowerUpgradeList = new TowerUpgradeList(newTowerProxy, _player, PaddingFromTop + fontHeight * 3f); 

            // Sell
            SellTowerButton sellButton = new SellTowerButton(PaddingFromTop + fontHeight * 3f + _currentTowerUpgradeList.Area.Height + PaddingFromTop * 2, newTowerProxy.Cost);
            sellButton.Click += (o, e) =>
            {
                _player.SellTower(newTowerProxy);
            };

            // Sell button must be added before tower upgrade list, otherwise the tower upgrade list tooltips
            // goes behind the sell button
            _basicUiContainer.Add(sellButton);
            _basicUiContainer.Add(_currentTowerUpgradeList);

            // DPS
            const int SwordTextureScale = 3;
            Texture2D swordTexture = FlaiGame.Current.Content.LoadTexture("Sword");
            float dpsWidth = font.MeasureString(newTowerProxy.DPS).X;

            UiTexture swordUiTexture = new UiTexture(swordTexture, new Vector2(screenSize.X - dpsWidth - swordTexture.Width * SwordTextureScale - HorizontalPadding * 2, PaddingFromTop + fontHeight * 1f)) { Scale = new Vector2(SwordTextureScale) };
            _basicUiContainer.Add(swordUiTexture);

            TextBlock dpsTextBlock = new TextBlock(newTowerProxy.DPS.ToString(), new Vector2(screenSize.X - dpsWidth - HorizontalPadding, PaddingFromTop + fontHeight * 1f)) { Font = FontName, IsCentered = false };
            _basicUiContainer.Add(dpsTextBlock);

            
        }

        #endregion

        #region SellTowerButton

        private class SellTowerButton : ButtonBase
        {
            private readonly int _sellValue;
            private readonly float _yCoordinate;

            public SellTowerButton(float yCoordinate, int sellValue)
            {
                _yCoordinate = yCoordinate;
                _sellValue = sellValue;
            }

            public override void Draw(GraphicsContext graphicsContext)
            {
                const string SellText = "SELL";
                const string FontName = "Minercraftory20";
                const int CoinTextureScale = 3;
                const int HorizontalPadding = 4;
                const int VerticalPadding = 4;
                const float BorderSize = 4;

                SpriteFont font = graphicsContext.FontContainer[FontName];
                Texture2D coinTexture = graphicsContext.ContentProvider.DefaultManager.LoadTexture("Coin");
                Vector2 sellValueStringSize = font.MeasureString(_sellValue);
                float sellStringWidth = font.GetStringWidth(SellText);

                float totalWidth = FlaiMath.Max(sellStringWidth + HorizontalPadding * 2, HorizontalPadding + sellValueStringSize.X + HorizontalPadding + coinTexture.Width * CoinTextureScale + HorizontalPadding);
                float totalHeight = sellValueStringSize.Y * 2 + VerticalPadding * 2;

                RectangleF area = new RectangleF(graphicsContext.ScreenSize.X - totalWidth - HorizontalPadding * 2, _yCoordinate, totalWidth, totalHeight);
                _area = area;

                // Background
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, area, IsPressedDown ? new Color(32, 32, 32) * 0.75f : new Color(72, 72, 72) * 0.5f);
                graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, new RectangleF(area).Inflate(BorderSize / 2, BorderSize / 2), Color.White, BorderSize);

                // Coin
                graphicsContext.SpriteBatch.Draw(coinTexture, new Vector2(area.X + HorizontalPadding, area.Y + VerticalPadding), Color.White, 0, CoinTextureScale);

                // Sell value
                graphicsContext.SpriteBatch.DrawString(font, _sellValue, new Vector2(area.X + HorizontalPadding + coinTexture.Width * CoinTextureScale, area.Y), Color.White);

                // "SELL"
                graphicsContext.SpriteBatch.DrawStringCentered(font, SellText, new Vector2(area.Center.X, area.Bottom - VerticalPadding - sellValueStringSize.Y / 2f), Color.White);
            }
        }

        #endregion

        #region Tower Upgrade List

        private class TowerUpgradeList : UiObject
        {
            private readonly TowerProxy _towerProxy;
            private readonly Player _player;
            private readonly float _yOffset;
            private readonly BasicUiContainer _uiContainer = new BasicUiContainer();
            private RectangleF _backgroundRectangle;

            public TowerUpgradeList(TowerProxy towerProxy, Player player, float yOffset)
            {
                _towerProxy = towerProxy;
                _player = player;
                _yOffset = yOffset;
                this.CreateUi();

                _player.CoinAmountChanged += OnPlayerCoinAmountChanged;
            }

            public override void Update(UpdateContext updateContext)
            {
                base.Update(updateContext);
                _uiContainer.Update(updateContext);
            }

            private void CreateUi()
            {
                const float Size = Tile.Size;
                float OffsetX = FlaiGame.Current.WindowSize.X - Size * 2 - 13;
                float OffsetY = _yOffset + 2;
                _uiContainer.Add(new TextBlock("UPGRADES", new Vector2(FlaiGame.Current.WindowSize.X - 54, _yOffset - 16)) { Font = "Minercraftory12" });

                TowerProxy[] upgrades = _towerProxy.UpgradePaths;
                TowerTooltip[] tooltips = new TowerTooltip[upgrades.Length];
                for (int i = 0; i < upgrades.Length; i++)
                {
                    int x = i % 2;
                    int y = i / 2;

                    TowerProxy upgrade = upgrades[i];

                    RectangleF area = new RectangleF(OffsetX + x * Size + x * 4, OffsetY + y * Size + y * 4, Size, Size);
                    TowerUpgradeButton button = new TowerUpgradeButton(area, upgrade) { Enabled = _player.Coins >= upgrade.Cost };

                    int cachedIndex = i;
                    button.Click += (o, e) =>
                    {
                        _player.UpgradeTower(_towerProxy, cachedIndex);
                    };

                    _uiContainer.Add(button);
                    tooltips[i] = new TowerTooltip(area, upgrade, RectangleCorner.TopRight);
                }

                foreach (TowerTooltip tooltip in tooltips)
                {
                    _uiContainer.Add(tooltip);
                }

                _backgroundRectangle = new RectangleF(OffsetX - 4, OffsetY - 4, Size * 2 + 12, Size * (1 + upgrades.Length / 2) + 12);
                _area = _backgroundRectangle;
            }

            public override void Draw(GraphicsContext graphicsContext)
            {
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, _backgroundRectangle, Color.Black * 0.5f);
                graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(graphicsContext, _backgroundRectangle, Color.White);

                _uiContainer.Draw(graphicsContext, true);
            }

            public void OnRemoved()
            {
                _player.CoinAmountChanged -= OnPlayerCoinAmountChanged;
            }

            private void OnPlayerCoinAmountChanged(int newCoinsAmount)
            {
                foreach (TowerUpgradeButton button in _uiContainer.FindAll<TowerUpgradeButton>())
                {
                    button.Enabled = newCoinsAmount >= button.UpgradeTower.Cost;
                }
            }

            #region Tower Button

            private class TowerUpgradeButton : ButtonBase
            {
                private readonly TowerProxy _towerProxy;
                public TowerProxy UpgradeTower
                {
                    get { return _towerProxy; }
                }

                public TowerUpgradeButton(RectangleF area, TowerProxy towerProxy)
                    : base(area)
                {
                    _towerProxy = towerProxy;
                }

                public override void Draw(GraphicsContext graphicsContext)
                {
                    TowerRenderer.DrawTowerBase(graphicsContext, _area.TopLeft, _towerProxy.TowerType, IsPressedDown ? Color.DarkGray : (Enabled ? Color.White : Color.Gray), Color.Black);
                }
            }

            #endregion
        }

        #endregion
    }
}
