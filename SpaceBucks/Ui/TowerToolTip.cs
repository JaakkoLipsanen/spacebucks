﻿using Flai;
using Flai.Extensions;
using Flai.Graphics;
using Flai.Ui;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceBucks.Model.Towers.TowerProxys;

namespace SpaceBucks.Ui
{
    // You could make an individual tool tip for each TowerToggleButton.
    // Actually making every UiObject have a possibility to have a 
    // ToolTip (inheritable class) could be pretty nice... Think about it
    public class TowerTooltip : UiObject
    {
        private readonly TowerProxy _towerProxy;
        private readonly RectangleCorner _anchorPoint;
        private Vector2? _mousePosition;

        public TowerTooltip(RectangleF area, TowerProxy towerProxy, RectangleCorner anchorPoint = RectangleCorner.TopLeft)
            : base(area)
        {
            _towerProxy = towerProxy;
            _anchorPoint = anchorPoint;
        }

        public override void Update(UpdateContext updateContext)
        {
            if (_area.Contains(updateContext.InputState.MousePosition))
            {
                _mousePosition = updateContext.InputState.MousePosition;
            }
            else
            {
                _mousePosition = null;
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            if (_mousePosition.HasValue)
            {
                const string FontName = "Minercraftory20";
                const float BorderThickness = 4f;
                const float Padding = 8f;
                const float SpaceAfterName = 6;
                const float PaddingFromScreenBorder = 4;

                SpriteFont font = graphicsContext.FontContainer[FontName];
                float nameWidth = font.GetStringWidth(_towerProxy.Name);
                float fontHeight = font.GetCharacterHeight();

                float width = nameWidth + Padding * 3;
                float height = fontHeight * 3 + Padding * 2 + SpaceAfterName; // * 3 because Name, Cost, DPS

                float x = (_anchorPoint == RectangleCorner.TopLeft || _anchorPoint == RectangleCorner.BottomLeft) ? _mousePosition.Value.X : _mousePosition.Value.X - width;
                float y = (_anchorPoint == RectangleCorner.TopLeft || _anchorPoint == RectangleCorner.TopRight) ? _mousePosition.Value.Y : _mousePosition.Value.Y - height;

                RectangleF area = new RectangleF(x, y, width, height);

                // Border
                graphicsContext.PrimitiveRenderer.DrawRectangleOutlines(
                    graphicsContext,
                    new RectangleF(area.X + BorderThickness / 2f, area.Y + BorderThickness / 2f, area.Width - BorderThickness, area.Height - BorderThickness),
                    Color.White, BorderThickness);

                // Tooltip blackish background with 0.5 opacity
                graphicsContext.PrimitiveRenderer.DrawRectangle(graphicsContext, area.Inflate(-BorderThickness, -BorderThickness), Color.Black * 0.5f);

                // Name
                graphicsContext.SpriteBatch.DrawString(font, _towerProxy.Name, new Vector2(area.X + Padding, area.Y + Padding), Color.White);

                const float TextureScale = 3;

                // Cost
                float costYPosition = area.Y + Padding + SpaceAfterName + fontHeight * 1;
                Texture2D coinTexture = graphicsContext.ContentProvider.DefaultManager.LoadTexture("Coin");
                graphicsContext.SpriteBatch.Draw(coinTexture, new Vector2(area.X + Padding, costYPosition), Color.White, 0, TextureScale);
                graphicsContext.SpriteBatch.DrawString(font, _towerProxy.Cost.ToString(),
                    new Vector2(area.X + Padding + coinTexture.Width * TextureScale + Padding, costYPosition - 4),
                    Color.White);

                // DPS
                float dpsYPosition = area.Y + Padding + SpaceAfterName + fontHeight * 2;
                Texture2D dpsTexture = graphicsContext.ContentProvider.DefaultManager.LoadTexture("Sword");
                graphicsContext.SpriteBatch.Draw(dpsTexture, new Vector2(area.X + Padding, dpsYPosition), Color.White, 0, TextureScale);
                graphicsContext.SpriteBatch.DrawString(font, _towerProxy.DPS.ToString(),
                    new Vector2(area.X + Padding + dpsTexture.Width * TextureScale + Padding, dpsYPosition),
                    Color.White);
            }
        }
    }
}
