﻿using System;
using System.Collections.Generic;
using System.Linq;
using Flai;
using Flai.Graphics;
using Flai.Ui;
using Microsoft.Xna.Framework;
using SpaceBucks.Model;
using SpaceBucks.Model.Towers;
using SpaceBucks.Model.Towers.TowerProxys;
using SpaceBucks.View;

namespace SpaceBucks.Ui
{
    public class TowerList : UiObject
    {
        private readonly Player _player;
        private readonly BasicUiContainer _uiContainer = new BasicUiContainer();

        public TowerList(Player player)
        {
            _player = player;
            this.CreateUi();
        }

        public override void Update(UpdateContext updateContext)
        {
            _uiContainer.Update(updateContext);
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            _uiContainer.Draw(graphicsContext, true);
        }

        private void CreateUi()
        {
            _uiContainer.Clear();

            const float OffsetX = 0;
            const float OffsetY = Tile.Size * 0.5f;
            const float Size = Tile.Size;

            List<TowerTooltip> tooltips = new List<TowerTooltip>();
            Dictionary<TowerType, TowerToggleButton> toggleButtons = new Dictionary<TowerType, TowerToggleButton>();
            TowerType[] towerTypes = Enum.GetValues(typeof(TowerType)).Cast<TowerType>().ToArray();

            int i = 0;
            foreach(TowerType type in towerTypes)
            {
                TowerProxy towerProxy = type.CreateTowerProxy();
                if (towerProxy == null)
                {
                    continue;
                }

                int x = i % 2;
                int y = i / 2;
                TowerToggleButton toggleButton = new TowerToggleButton(new RectangleF(OffsetX + x * Size, OffsetY + y * Size, Size, Size), towerProxy); 
                tooltips.Add(new TowerTooltip(toggleButton.Area, toggleButton.TowerProxy));
                toggleButton.Click += (o, e) =>
                {
                    if (_player.CurrentlyAddingTower != null && _player.CurrentlyAddingTower.TowerType == type)
                    {
                        toggleButton.IsToggled = false;
                        _player.CurrentlyAddingTower = null;
                    }
                    else
                    {
                        if (_player.CurrentlyAddingTower != null)
                        {
                            toggleButtons[_player.CurrentlyAddingTower.TowerType].IsToggled = false;
                        }

                        toggleButton.IsToggled = true;
                        _player.CurrentlyAddingTower = toggleButtons[type].TowerProxy;
                    }
                };

                _player.CoinAmountChanged += (newCoinAmount) =>
                {
                    toggleButton.Enabled = newCoinAmount >= toggleButton.TowerProxy.Cost;
                    if (!toggleButton.Enabled)
                    {
                        toggleButton.IsToggled = false;
                    }
                };

                toggleButtons.Add(type, toggleButton);
                _uiContainer.Add(toggleButton);
                i++;
            }

            foreach(TowerTooltip tooltip in tooltips)
            {
                _uiContainer.Add(tooltip);
            }

            _player.CurrentlyAddingTowerChanged += (oldTowerProxy, newTowerProxy) =>
            {
                if (oldTowerProxy != null)
                {
                    toggleButtons[oldTowerProxy.TowerType].IsToggled = false;
                }

                if (newTowerProxy != null)
                {
                    toggleButtons[newTowerProxy.TowerType].IsToggled = true;
                }
            };
        }

        #region Tower Toggle Button

        private class TowerToggleButton : ToggleButtonBase
        {
            private readonly TowerProxy _towerProxy;
            public TowerProxy TowerProxy
            {
                get { return _towerProxy; }
            }

            public TowerToggleButton(RectangleF area, TowerProxy towerProxy)
                : base(area, false)
            {
                _towerProxy = towerProxy;
            }

            public override void Draw(GraphicsContext graphicsContext)
            {
                TowerRenderer.DrawTowerBase(graphicsContext, _area.TopLeft, _towerProxy.TowerType, IsToggled ? Color.Black * 0.35f : (Enabled ? Color.White : Color.Gray), IsToggled ? Color.White : Color.Black);
            }
        }

        #endregion
    }
}
