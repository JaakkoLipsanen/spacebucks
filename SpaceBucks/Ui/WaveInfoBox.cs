﻿using Flai;
using Flai.Graphics;
using Flai.Ui;
using Microsoft.Xna.Framework;
using SpaceBucks.Model;

namespace SpaceBucks.Ui
{
    public class WaveInfoBox : UiObject
    {
        private readonly BasicUiContainer _basicUiContainer = new BasicUiContainer();
        private readonly World _world;

        public WaveInfoBox(World world)
            : base(new Rectangle((world.Map.Width - 10) * Tile.Size, (world.Map.Height - 2) * Tile.Size, Tile.Size * 10, Tile.Size * 2))
        {
            _world = world;
            this.CreateInnerUiElements();
        }

        public override void Update(UpdateContext updateContext)
        {
            base.Update(updateContext);
            _basicUiContainer.Update(updateContext);
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            base.Draw(graphicsContext);
            _basicUiContainer.Draw(graphicsContext, true);
        }

        private void CreateInnerUiElements()
        {
            TextBlock waveTextBlock = new TextBlock("WAVE 1", new Vector2(_area.X + _area.Width / 6 * 5, _area.Y + _area.Height / 4)) { Color = Color.Gray, Font = "Minercraftory16" };
            TextButton startWaveButton = new TextButton("START", new Vector2(_area.X + _area.Width / 6 * 5, _area.Y + _area.Height / 7 * 5));

            // Start wave button clicked
            startWaveButton.Click += (o, e) =>
            {
                _world.CurrentWave.Start();
                startWaveButton.Enabled = false;

                waveTextBlock.Color = Color.White;
            };

            // Wave completed
            _world.WaveCompleted += (waveIndex, completionCoins) =>
            {
                startWaveButton.Enabled = true;
                waveTextBlock.Color = Color.Gray;
                waveTextBlock.Text = "WAVE " + _world.CurrentWaveIndex;
            };

            _basicUiContainer.Add(waveTextBlock);
            _basicUiContainer.Add(startWaveButton);
        }
    }
}
