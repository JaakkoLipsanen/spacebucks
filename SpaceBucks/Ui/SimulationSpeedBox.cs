﻿using Flai;
using Flai.Graphics;
using Flai.Ui;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Ui
{
    public class SimulationSpeedControl : UiObject
    {
        private const int MaxSimulationSpeed = 16;

        private readonly BasicUiContainer _uiContainer = new BasicUiContainer();
        private int _simulationSpeed = 1;

        public int SimulationSpeed
        {
            get { return _simulationSpeed; }
            set { _simulationSpeed = value; }
        }

        public SimulationSpeedControl()
        {
            this.CreateUi();
        }

        public override void Update(UpdateContext updateContext)
        {
            _uiContainer.Update(updateContext);
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            _uiContainer.Draw(graphicsContext, true);
        }

        private void CreateUi()
        {
            float OffsetX = FlaiGame.Current.WindowSize.X - 260;
            float OffsetY = FlaiGame.Current.WindowSize.Y + 8;

            const string FontPlusMinusName = "MineCrafter20";

            // "SPEED" header
            TextBlock speedTextBlock = new TextBlock("SPEED", new Vector2(OffsetX + 36, OffsetY - 64)) { Font = "Minercraftory16" };
            _uiContainer.Add(speedTextBlock);

            // "1x" etc multiplier text
            TextBlock speedMultiplierTextBlock = new TextBlock(_simulationSpeed + "x", new Vector2(OffsetX + 36, OffsetY - 28)) { Font = "Minercraftory20" };
            _uiContainer.Add(speedMultiplierTextBlock);

            // "-" button
            TextButton minusButton = new TextButton("-", new Vector2(OffsetX, OffsetY)) { Font = FontPlusMinusName };         
            _uiContainer.Add(minusButton);
            
            // "+" button
            TextButton plusButton = new TextButton("+", new Vector2(OffsetX + 84, OffsetY)) { Font = FontPlusMinusName };      
            _uiContainer.Add(plusButton);

            // Minus button Click event handler
            minusButton.Click += (o, e) =>
            {
                _simulationSpeed /= 2;
                if (_simulationSpeed == 0)
                {
                    minusButton.Enabled = false;
                }
                else if (_simulationSpeed == MaxSimulationSpeed / 2)
                {
                    plusButton.Enabled = true;
                }

                speedMultiplierTextBlock.Text = _simulationSpeed + "x";
            };

            // Plus button Click event handler
            plusButton.Click += (o, e) =>
            {
                _simulationSpeed = (_simulationSpeed == 0) ? 1 : _simulationSpeed * 2;
                if (_simulationSpeed == 1)
                {
                    minusButton.Enabled = true;
                }
                else if (_simulationSpeed == MaxSimulationSpeed)
                {
                    plusButton.Enabled = false;
                }

                speedMultiplierTextBlock.Text = _simulationSpeed + "x";
            }; 
        }
    }
}
