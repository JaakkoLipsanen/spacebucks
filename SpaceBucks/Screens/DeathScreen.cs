﻿using System;
using Flai;
using Flai.Graphics;
using Flai.ScreenManagement;
using Flai.Ui;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Screens
{
    public class DeathScreen : GameScreen
    {
        private readonly BasicUiContainer _uiContainer = new BasicUiContainer();
        private readonly IGameResetHandle _gameResetHandle;

        public override bool IsPopup
        {
            get { return true; }
        }

        public DeathScreen(IGameResetHandle resetHandle)
        {
            _gameResetHandle = resetHandle;

            base.TransitionOnTime = TimeSpan.FromSeconds(0.5f);
            base.TransitionOffTime = TimeSpan.FromSeconds(0.5f);
            base.FadeBackBufferToBlack = false;
        }

        protected override void LoadContent()
        {
            this.CreateUi();
        }

        protected override void Update(UpdateContext updateContext, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            if (this.IsActive)
            {
                _uiContainer.Update(updateContext);
            }

            foreach (TextBlock textBlock in _uiContainer.FindAll<TextBlock>())
            {
                textBlock.Color = Color.White * base.TransitionAlpha;
            }

            foreach (TextButton textButton in _uiContainer.FindAll<TextButton>())
            {
                textButton.Color = Color.White * base.TransitionAlpha;
            }
        }

        protected override void Draw(GraphicsContext graphicsContext)
        {
            // TODO: instead of black background, add black-and-white filter. However, I'm not quite sure how that should be done
            // since the post-processing is currently at the SpaceBucksGame.cs and... idk, figure it out. Or don't, that's not really necessary
            graphicsContext.SpriteBatch.Begin();
            graphicsContext.SpriteBatch.DrawFullscreen(graphicsContext.BlankTexture, Color.Black * 0.5f * base.TransitionAlpha);
            graphicsContext.SpriteBatch.End();

            _uiContainer.Draw(graphicsContext);
        }

        private void CreateUi()
        {
            _uiContainer.Add(new TextBlock("GAME OVER", new Vector2(base.Game.WindowSize.X / 2f, base.Game.WindowSize.Y / 4)));

            TextButton playAgainButton = new TextButton("PLAY AGAIN?", new Vector2(base.Game.WindowSize.X / 4, base.Game.WindowSize.Y / 2));
            playAgainButton.Click += (o, e) =>
            {
                _gameResetHandle.Reset();
                this.ExitScreen();
            };
            _uiContainer.Add(playAgainButton);

            TextButton exitButton = new TextButton("EXIT", new Vector2(base.Game.WindowSize.X / 4 * 3, base.Game.WindowSize.Y / 2));
            exitButton.Click += (o, e) =>
            {
                base.Game.Exit();
            };
            _uiContainer.Add(exitButton);
        }
    }
}
