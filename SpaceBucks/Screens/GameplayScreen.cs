﻿using Flai;
using Flai.Graphics;
using Flai.ScreenManagement;
using Flai.Ui;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SpaceBucks.Controller;
using SpaceBucks.Model;
using SpaceBucks.Ui;
using SpaceBucks.View;

namespace SpaceBucks.Screens
{
    public interface IGameResetHandle
    {
        void Reset();
    }

    public class GameplayScreen : GameScreen, IGameResetHandle
    {
        private readonly BasicUiContainer _uiContainer = new BasicUiContainer();
        private SimulationSpeedControl _simulationSpeedControl;

        private Level _level;
        private LevelRenderer _levelRenderer;
        private LevelController _levelController;

        private bool _isPaused = false;

        public GameplayScreen()
        {
            _level = Level.Generate();
            _levelRenderer = new LevelRenderer(_level);
            _levelController = new LevelController(_level);
            _level.Player.PlayerDied += OnPlayerDied;

            this.Reset();
        }

        protected override void LoadContent()
        {
            if (_levelRenderer != null)
            {
                _levelRenderer.LoadContent();
            }
        }

        protected override void Update(UpdateContext updateContext, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            if(updateContext.InputState.IsNewKeyPress(Keys.Escape))
            {
                _isPaused = !_isPaused;
            }

            if (!_isPaused && base.Game.IsActive)
            {
                // Handle input only if the screen has focus
                if (this.IsActive)
                {
                    _uiContainer.Update(updateContext);
                    _levelController.Control(updateContext);
                }

                for (int i = 0; i < _simulationSpeedControl.SimulationSpeed; i++)
                {
                    _level.Update(updateContext);
                    _levelRenderer.Update(updateContext);
                }
            }
        }

        protected override void Draw(GraphicsContext graphicsContext)
        {
            // Draw level
            _levelRenderer.Draw(graphicsContext);

            // Draw UI
            graphicsContext.SpriteBatch.Begin(SamplerState.PointClamp);
            _uiContainer.Draw(graphicsContext, true);
            graphicsContext.SpriteBatch.End();
        }

        public void Reset()
        {
            _level.Reset();
            this.CreateUi();
        }

        private void CreateUi()
        {
            _uiContainer.Clear();

            _uiContainer.Add(new WaveInfoBox(_level.World));
            _uiContainer.Add(new TowerList(_level.Player));
            _uiContainer.Add(new TowerInfoBox(_level.Player));
            _uiContainer.Add(_simulationSpeedControl = new SimulationSpeedControl());
        }

        private void OnPlayerDied()
        {
            base.ScreenManager.AddScreen(new DeathScreen(this));
        }
    }
}
