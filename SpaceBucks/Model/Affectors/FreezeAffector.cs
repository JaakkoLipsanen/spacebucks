﻿
using Microsoft.Xna.Framework;
namespace SpaceBucks.Model.Affectors
{
    // TODO: Make better
    public class FreezeAffector : Affector
    {
        private readonly float _slowMultiplier;
        private readonly float _dps;

        protected override float DamagePerSecond
        {
            get { return _dps; }
        }

        // BLUE
        public override Color Color
        {
            get { return new Color(65, 105, 255); }
        }

        public override float ProcessSpeed(float inputSpeed)
        {
            return inputSpeed * _slowMultiplier;
        }

        public FreezeAffector(float time, float slowMultiplier, float dps)
            : base(time)
        {
            _slowMultiplier = slowMultiplier;
            _dps = dps;
        }

        public override int CompareTo(Affector other)
        {
            FreezeAffector otherFreezeAffector = other as FreezeAffector;
            if (otherFreezeAffector != null)
            {
                return -(_slowMultiplier.CompareTo(otherFreezeAffector._slowMultiplier)); // Lower value should return "higher"/better (== 1)
            }

            return 0;
        }
    }
}
