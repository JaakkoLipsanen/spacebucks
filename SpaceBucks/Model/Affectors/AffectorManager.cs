﻿using System.Collections.Generic;
using Flai;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Model.Affectors
{
    public class AffectorManager
    {
        private readonly List<Affector> _affectors = new List<Affector>();
        public Color EnemyTint
        {
            get
            {
                if (_affectors.Count == 0)
                {
                    return Color.White;
                }

                int r = 0, g = 0, b = 0;
                foreach (Affector affector in _affectors)
                {
                    r += affector.Color.R;
                    g += affector.Color.G;
                    b += affector.Color.B;
                }

                return new Color(r / _affectors.Count, g / _affectors.Count, b / _affectors.Count);
            }
        }

        public void Update(UpdateContext updateContext)
        {
            foreach (Affector affector in _affectors)
            {
                affector.Update(updateContext);
            }

            _affectors.RemoveAll(affector => affector.IsCompleted);
        }

        public void AddAffector<T>(T newAffector)
            where T : Affector
        {
            // Should there be something like a queue? Or choose the affector that is more powerful etc?
            if (!newAffector.IsStacking)
            {
                // Okay this isn't actually very good solution considering that "FreezeAffector" and 
                // "ClassThatInheritsFromFreezeAffector" are different types but usually should be "considered"
                // as one. But once again, whatever. And no need to RemoveAll(), since if the Affector is not stacking,
                // then there should be only one (if any)
                int index = _affectors.FindIndex(affector => affector is T);
                if (index >= 0)
                {
                    T affector = _affectors[index] as T;
                    int compareResult = newAffector.CompareTo(affector);
                    if (compareResult != -1)
                    {
                        _affectors.RemoveAt(index);
                        _affectors.Add(newAffector);
                    }

                    return;
                }
            }

            _affectors.Add(newAffector);
        }

        public float ProcessDamageInput(float inputDamage)
        {
            foreach (Affector affector in _affectors)
            {
                inputDamage = affector.ProcessDamageInput(inputDamage);
            }

            return inputDamage;
        }

        public float ProcessSpeed(float inputSpeed)
        {
            foreach (Affector affector in _affectors)
            {
                // Okay.. This is where this gets wrong.
                // Basically, the end result now depends on the order of the effects.
                // If for example Affector1 decreases the speed by 20 and Affector2 
                // multiplies the speed by 0.5, then "(100 - 20) * 0.5 = 40" and "100 * 0.5 - 20 = 30".
                // This affects every thing that Affectors can affect, but since this game will not actually be released
                // nor does this thing really change anything, lets not think about it. Just a FYI
                inputSpeed = affector.ProcessSpeed(inputSpeed);
            }

            return inputSpeed;
        }
    }
}
