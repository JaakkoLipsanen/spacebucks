﻿
using System;
using Flai;
using Microsoft.Xna.Framework;
namespace SpaceBucks.Model.Affectors
{
    // Affector class represents effects that enemies can have. For example "frozen"/slow effect
    // or "take increased damage effect" or "burning flame" effect etc
    public abstract class Affector : IComparable<Affector>
    {
        private Enemy _target;
        public Enemy Target
        {
            get { return _target; }
            set
            {
                if (_target != null)
                {
                    throw new ArgumentException("Affector can't be re-attached");
                }

                _target = value;
            }
        }

        // Should there be other options for the duration than time? I could make
        // a abstract "AffectorDuration" class?
        private float _remainingTime;

        public float RemainingTime
        {
            get { return _remainingTime; }
        }

        public virtual bool IsCompleted
        {
            get { return _remainingTime <= 0 || !_target.IsAlive || _target.HasCompleted; }
        }

        protected virtual float DamagePerSecond { get { return 0f; } }
        public virtual Color Color { get { return Color.White; } }

        // This is a bit "ugly", since it should basically be a virtual *static* property.
        // That is because the value should be same for all "FreezeAffector"'s etc and not depend
        // on the values of the instance of FreezeAffector etc. But whatever
        public virtual bool IsStacking { get { return false; } }

        // Add more ProcessXXX?
        public virtual float ProcessDamageInput(float damageInput) { return damageInput; }
        public virtual float ProcessSpeed(float inputSpeed) { return inputSpeed; }

        public Affector(float time)
        {
            _remainingTime = time;
        }

        // In Update method, the effect can do stuff like "take constant damage" etc
        // EDIT: Actually, let's make it not-virtual for now since I added the "DPS" property
        public void Update(UpdateContext updateContext)
        {
            _remainingTime -= updateContext.DeltaSeconds;
            if (this.DamagePerSecond != 0)
            {
                _target.TakeDamage(this.DamagePerSecond * updateContext.DeltaSeconds);
            }
        }

        public virtual int CompareTo(Affector other)
        {
            return 0;
        }
    }
}
