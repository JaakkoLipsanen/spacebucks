﻿using Microsoft.Xna.Framework;

namespace SpaceBucks.Model.Affectors
{
    // High initial DPS, lowers lineary
    public class FireAffector : Affector
    {
        private readonly float _dps;
        private readonly float _totalTime;

        protected override float DamagePerSecond
        {
            get { return _dps * (this.RemainingTime / _totalTime); }
        }

        // RED
        public override Color Color
        {
            get { return new Color(255, 105, 65); }
        }

        public FireAffector(float time, float dps)
            : base(time)
        {
            _totalTime = time;
            _dps = dps;
        }

        public override int CompareTo(Affector other)
        {
            FireAffector otherFireAffector = other as FireAffector;
            if (otherFireAffector != null)
            {
                return _dps.CompareTo(otherFireAffector._dps);
            }

            return 0;
        }
    }
}
