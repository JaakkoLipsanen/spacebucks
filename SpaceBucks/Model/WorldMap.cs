﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Flai;
using Microsoft.Xna.Framework;
using SpaceBucks.Model.Towers;
using SpaceBucks.Model.Towers.TowerProxys;

namespace SpaceBucks.Model
{
    [Flags]
    public enum AlternateAxis
    {
        None,
        X,
        Y,
        Both = X | Y
    }

    public class WorldMap
    {
        private const int MinimumWidth = 8;
        private const int MinimumHeight = 8;

        private readonly int _width;
        private readonly int _height;
        private readonly bool[] _isSolidMap;
        private readonly bool[] _isBuildableMap;

        private readonly Vector2[] _enemyPath; // Path in tile-coordinates
        private readonly AlternateAxis _enemyPathAlternateAxises;

        private readonly List<TowerProxy> _towerProxys;
        private readonly ReadOnlyCollection<TowerProxy> _readOnlyTowerProxys;

        public int Width
        {
            get { return _width; }
        }

        public int Height
        {
            get { return _height; }
        }

        public ReadOnlyCollection<TowerProxy> TowerProxys
        {
            get { return _readOnlyTowerProxys; }
        }

        public IEnumerable<Tower> Towers
        {
            get { return _readOnlyTowerProxys.Select(proxy => proxy.Tower); }
        }

        public WorldMap(int width, int height, bool[] isSolidMap, bool[] isBuildableMap, Vector2[] enemyPath, AlternateAxis enemyPathAlternateAxises)
        {
            if (width < MinimumWidth || height < MinimumHeight)
            {
                throw new ArgumentOutOfRangeException("The size of the map is too small");
            }
            else if (isSolidMap.Length != width * height)
            {
                throw new ArgumentException("The isSolidMap's length does not match width and height");
            }
            else if (isSolidMap.Length != isBuildableMap.Length)
            {
                throw new ArgumentException("The isBuildableMap's and isSolidMap's length doesn't match!");
            }

            _width = width;
            _height = height;
            _isSolidMap = isSolidMap;
            _isBuildableMap = isBuildableMap;
            _enemyPath = enemyPath;
            _enemyPathAlternateAxises = enemyPathAlternateAxises;

            _towerProxys = new List<TowerProxy>();
            _readOnlyTowerProxys = new ReadOnlyCollection<TowerProxy>(_towerProxys);
        }

        #region IsSolid/IsFree

        //public void SetIsFree(int x, int y, bool value)
        //{
        //    if (x < 0 || y < 0 || x >= _width || y >= _height)
        //    {
        //        return;
        //    }

        //    _isSolidMap[x + y * _width] = value;
        //}

        public bool IsSolid(int x, int y)
        {
            return this.IsSolid(new Vector2i(x, y));
        }

        public bool IsSolid(Vector2i v)
        {
            if (v.X < 0 || v.Y < 0 || v.X >= _width || v.Y >= _height)
            {
                return false;
            }

            return _isSolidMap[v.X + v.Y * _width];
        }

        public bool IsFree(int x, int y)
        {
            return this.IsFree(new Vector2i(x, y));
        }

        public bool IsFree(Vector2i v)
        {
            if (!this.IsSolid(v))
            {
                return false;
            }

            // check if any of the towers exists in the index
            foreach (Tower tower in this.Towers)
            {
                if (tower.WorldIndex == v)
                {
                    return false;
                }
            }

            return _isBuildableMap[v.X + v.Y * _width];
        }

        public bool IsBuildable(Vector2i v)
        {
            return _isBuildableMap[v.X + v.Y * _width];
        }

        #endregion

        #region Add/Get/Remove Tower

        public bool AddTower(TowerProxy towerProxy, Vector2i worldIndex)
        {
            if (towerProxy == null)
            {
                FlaiLogger.Log("WorldMap.AddTower: Could not add tower, because the tower was null");
                return false;
            }

            if (!this.IsFree(worldIndex))
            {
                FlaiLogger.Log("WorldMap.AddTower: Could not add tower, because the index is already taken!");
                return false;
            }

            if (_towerProxys.Contains(towerProxy))
            {
                FlaiLogger.Log("WorldMap.AddTower: Could not add tower, because the tower already exists in the world");
                return false;
            }

            _towerProxys.Add(towerProxy);
            return true;
        }

        public TowerProxy GetTowerProxyAt(int x, int y)
        {
            return this.GetTowerProxyAt(new Vector2i(x, y));
        }

        public TowerProxy GetTowerProxyAt(Vector2i index)
        {
            foreach (TowerProxy towerProxy in _towerProxys)
            {
                if (towerProxy.Tower.WorldIndex == index)
                {
                    return towerProxy;
                }
            }

            FlaiLogger.Log("WorldMap.GetTowerAt: Could not get tower, because no tower exists at {0}", index);
            return null;
        }

        public Tower GetTowerAt(int x, int y)
        {
            TowerProxy proxy = this.GetTowerProxyAt(new Vector2i(x, y));
            return proxy == null ? null : proxy.Tower;
        }

        public Tower GetTowerAt(Vector2i index)
        {
            TowerProxy proxy = this.GetTowerProxyAt(index);
            return proxy == null ? null : proxy.Tower;
        }

        public bool TryGetTowerAt(int x, int y, out Tower tower)
        {
            return this.TryGetTowerAt(new Vector2i(x, y), out tower);
        }

        public bool TryGetTowerAt(Vector2i index, out Tower tower)
        {
            foreach (Tower currentTower in this.Towers)
            {
                if (currentTower.WorldIndex == index)
                {
                    tower = currentTower;
                    return true;
                }
            }

            FlaiLogger.Log("WorldMap.GetTowerAt: Could not get tower, because no tower exists at {0}", index);
            tower = null;
            return false;
        }

        public bool RemoveTower(Tower tower)
        {
            int index = _towerProxys.FindIndex(proxy => proxy.Tower == tower);
            if (index < 0)
            {
                FlaiLogger.Log("WorldMap.RemoveTower: Could not remove tower, because the tower is not added to the world");
                return false;
            }

            _towerProxys.RemoveAt(index);
            return true;
        }

        public bool RemoveTower(Vector2i index)
        {
            Tower tower;
            return this.RemoveTower(index, out tower);
        }

        public bool RemoveTower(Vector2i index, out Tower tower)
        {
            for(int i = 0; i < _towerProxys.Count; i++)
            {
                if (_towerProxys[i].Tower.WorldIndex == index)
                {
                    tower = _towerProxys[i].Tower;
                    _towerProxys.RemoveAt(i);
                    return true;
                }
            }

            FlaiLogger.Log("WorldMap.RemoveTower: Could not remove tower, because there is no tower at the index {0}", index);
            tower = null;
            return false;
        }

        #endregion

        public IEnumerator<Vector2> GetPath(int enemyIndex)
        {
            for (int i = 0; i < _enemyPath.Length; i++)
            {
                Vector2 tileCoordinate = _enemyPath[i];
                if (enemyIndex % 2 == 0)
                {
                    if (_enemyPathAlternateAxises.HasFlag(AlternateAxis.X))
                    {
                        tileCoordinate.X = _width - tileCoordinate.X;
                    }

                    if (_enemyPathAlternateAxises.HasFlag(AlternateAxis.Y))
                    {
                        tileCoordinate.Y = _height - tileCoordinate.Y;
                    }
                }

                yield return tileCoordinate * Tile.Size;
            }
        }

        public void Reset()
        {
            _towerProxys.Clear();
        }
    }
}
