﻿
using Flai;
using Flai.DataStructures;
using Microsoft.Xna.Framework;
using System;

namespace SpaceBucks.Model
{
    // TODO: Add some color change? For example that some stars can be a bit reddish, some bluish etc
    public class Star : IComparable<Star>
    {
        private static readonly Vector2 AxisSpeedMultiplier = new Vector2(10f, 2f);
        public const int BaseSize = 12;

        private readonly Vector2 _startingPosition;
        private readonly float _scale;
        private readonly float _distance;
        private readonly float _brightness;

        public float Size
        {
            get { return _distance * _scale * Star.BaseSize; }
        }

        public float Brightness
        {
            get { return _brightness * _scale; }
        }

        public Vector2 Position { get; private set; }

        public Star(Vector2 defaultPosition, float scale, float distance, float brightness)
        {
            _scale = scale;
            _distance = distance;
            _brightness = brightness;

            _startingPosition = defaultPosition;
        }

        public void Update(float totalMovementTime, Vector2i wrappingAreaSize)
        {
            this.Position = new Vector2(
                FlaiMath.RealModulus(_startingPosition.X - totalMovementTime * _distance * Star.AxisSpeedMultiplier.X, wrappingAreaSize.X),
                FlaiMath.RealModulus(_startingPosition.Y - totalMovementTime * _distance * Star.AxisSpeedMultiplier.Y, wrappingAreaSize.Y));
        }

        public int CompareTo(Star other)
        {
            return _distance.CompareTo(other._distance);
        }
    }

    // planets etc?
    public class StarField
    {
        private const float Density = 0.5f;

        private readonly Star[] _stars;
        private readonly ReadOnlyArray<Star> _readOnlyStars;

        private readonly Vector2i _wrappingAreaSize;
        private float _totalTime = 0f;

        public ReadOnlyArray<Star> Stars
        {
            get { return _readOnlyStars; }
        }

        public StarField(Vector2i wrappingAreaSize)
            : this(wrappingAreaSize, Environment.TickCount)
        {
        }

        public StarField(Vector2i wrappingAreaSize, int seed)
        {
            _wrappingAreaSize = wrappingAreaSize;
            _stars = this.GenerateStars(seed);
            _readOnlyStars = new ReadOnlyArray<Star>(_stars);
        }

        public void Update(UpdateContext updateContext)
        {
            _totalTime += updateContext.DeltaSeconds;
            foreach (Star star in _stars)
            {
                star.Update(_totalTime, _wrappingAreaSize);
            }
        }

        private Star[] GenerateStars(int seed)
        {
            int gridWidth = (int)(_wrappingAreaSize.X / (Star.BaseSize * 4) * Density);
            int gridHeight = (int)(_wrappingAreaSize.Y / (Star.BaseSize * 4) * Density);

            float cellWidth = _wrappingAreaSize.X / (float)gridWidth;
            float cellHeight = _wrappingAreaSize.Y / (float)gridHeight;

            FlaiRandom random = new FlaiRandom(seed);
            Star[] stars = new Star[gridWidth * gridHeight];
            for (int y = 0; y < gridHeight; y++)
            {
                for (int x = 0; x < gridWidth; x++)
                {
                    Vector2 position = new Vector2(
                        random.NextFloat(cellWidth * x, cellWidth * (x + 1)),
                        random.NextFloat(cellHeight * y, cellHeight * (y + 1)));

                    float scale = random.NextFloat(0.5f, 1.1f);
                    float brightness = random.NextFloat(0.3f, 0.9f);
                    float distance = random.NextFloat(0.2f, 0.95f);

                    stars[x + y * gridWidth] = new Star(position, scale, distance, brightness);
                }
            }

            // sort the stars based on their distance
            // so that they are automatically drawn
            // from back to front
            Array.Sort(stars);
            return stars;
        }
    }
}
