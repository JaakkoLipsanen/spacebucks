﻿using Flai;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Model
{
    public class Wave
    {
        private readonly IWaveData _waveData;
        private readonly World _world;
        private readonly List<Enemy> _enemies;
        private readonly ReadOnlyCollection<Enemy> _readOnlyEnemies;

        private int _spawnedEnemies = 0;
        private float _timeSinceStart = 0f;
        private bool _isRunning = false;

        public bool IsCompleted
        {
            get { return _spawnedEnemies == _waveData.EnemyCount && _enemies.Count == 0; }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
        }

        public ReadOnlyCollection<Enemy> Enemies
        {
            get { return _readOnlyEnemies; }
        }

        public Wave(IWaveData waveData, World world)
        {
            _waveData = waveData;
            _world = world;

            _enemies = new List<Enemy>();
            _readOnlyEnemies = new ReadOnlyCollection<Enemy>(_enemies);
        }

        public void Update(UpdateContext updateContext)
        {
            if (!_isRunning)
            {
                return;
            }

            _timeSinceStart += updateContext.DeltaSeconds;
            if (_spawnedEnemies < _waveData.EnemyCount)
            {
                this.SpawnNewEnemies();
            }

            foreach (Enemy enemy in _enemies)
            {
                if (enemy.IsAlive)
                {
                    enemy.Update(updateContext);
                }
            }

            foreach (Enemy enemy in _enemies.Where(enemy => !enemy.IsAlive))
            {
                _world.OnEnemyKilled(enemy);
            }

            // death animation etc?
            _enemies.RemoveAll(enemy => !enemy.IsAlive);

            foreach (Enemy enemy in _enemies)
            {
                if (enemy.HasCompleted)
                {
                    _world.OnEnemyThrough(enemy);
                }
            }

            _enemies.RemoveAll(enemy => enemy.HasCompleted);
        }

        public void Start()
        {
            if (_isRunning)
            {
                FlaiLogger.Log("Wave.Start: The wave is already running");
            }

            _isRunning = true;
        }

        public Enemy FindNearestEnemy(Vector2 position, float maximumRange = float.MaxValue)
        {
            if (!this.IsRunning)
            {
                return null;
            }

            int indexOfNearestEnemy = -1;
            float distanceToNearestEnemy = float.MaxValue;
            for (int i = 0; i < _enemies.Count; i++)
            {
                Enemy enemy = _enemies[i];
                if (enemy.IsAlive && !enemy.HasCompleted)
                {
                    float distance = Vector2.Distance(enemy.Position, position);
                    if (distance < distanceToNearestEnemy && distance < maximumRange)
                    {
                        indexOfNearestEnemy = i;
                        distanceToNearestEnemy = distance;
                    }
                }
            }

            if (indexOfNearestEnemy != -1)
            {
                return _enemies[indexOfNearestEnemy];
            }

            return null;
        }

        public Enemy FindOptimalEnemy(Vector2 position, float maximumRange = float.MaxValue)
        {
            // You could make a better algorithm (the enemy thats near goal and/or has less health)
            return this.FindNearestEnemy(position, maximumRange);
        }

        public IEnumerable<Enemy> FindAllEnemiesWithinRange(Vector2 position, float maximumRange)
        {
            foreach (Enemy enemy in _enemies)
            {
                if (enemy.IsAlive && !enemy.HasCompleted && Vector2.Distance(position, enemy.Position) < maximumRange)
                {
                    yield return enemy;
                }
            }
        }

        private void SpawnNewEnemies()
        {
            while (_timeSinceStart > _spawnedEnemies * _waveData.TimeBetweenEnemySpawns && _spawnedEnemies < _waveData.EnemyCount)
            {
                _enemies.Add(_waveData.CreateEnemy(_spawnedEnemies)); // Should CreateEnemy take the enemy's path as an parameter?
                _spawnedEnemies++;
            }
        }
    }
}
