﻿
using System;
using Flai;
using SpaceBucks.Model.Towers.TowerProxys;
namespace SpaceBucks.Model
{
    public delegate void CurrentlyAddingTowerProxyChangedEvent(TowerProxy oldTowerProxy, TowerProxy newTowerProxy);
    public delegate void SelectedTowerChangedEvent(TowerProxy newTowerProxy);
    public delegate void CoinAmountChangedEvent(int newCoinsAmount);
    public delegate void PlayerDiedEvent();

    public class Player
    {
        #region Fields

        public const int StartingCoins = 70;
        public const int MaximumLives = 10;
        public event CurrentlyAddingTowerProxyChangedEvent CurrentlyAddingTowerChanged;
        public event SelectedTowerChangedEvent SelectedTowerChanged;
        public event CoinAmountChangedEvent CoinAmountChanged;
        public event PlayerDiedEvent PlayerDied; // Horrible name imo

        private readonly World _world; 
        private Vector2i _mouseIndex = Vector2i.Zero;

        private int _currentLives = Player.MaximumLives;
        private int _coins = Player.StartingCoins;

        private TowerProxy _currentlyAddingTower;
        private TowerProxy _selectedTower;

        #endregion

        #region Properties

        public int Lives
        {
            get { return _currentLives; }
        }

        public bool IsAlive
        {
            get { return _currentLives > 0; }
        }

        public int Coins
        {
            get { return _coins; }
            private set
            {
                if (_coins != value)
                {
                    if (value < 0)
                    {
                        throw new ArgumentOutOfRangeException("Coins must have a positive value");
                    }

                    _coins = value;
                    if (CoinAmountChanged != null)
                    {
                        CoinAmountChanged(_coins);
                    }
                }
            }
        }

        // Should this thing be a tool..? Kinda like DarkLight's editor's tools
        public TowerProxy CurrentlyAddingTower
        {
            get { return _currentlyAddingTower; }
            set
            {
                if (_currentlyAddingTower != value)
                {
                    if (value != null && value.Cost > _coins)
                    {
                        return;
                    }

                    TowerProxy oldTower = _currentlyAddingTower;
                    _currentlyAddingTower = value;

                    if (CurrentlyAddingTowerChanged != null)
                    {
                        CurrentlyAddingTowerChanged(oldTower, _currentlyAddingTower);
                    }

                    if (_currentlyAddingTower != null)
                    {
                        this.SelectedTower = null;
                    }
                }
            }
        }

        public TowerProxy SelectedTower
        {
            get { return _selectedTower; }
            set
            {
                if (_selectedTower != value)
                {
                    if (_selectedTower != null)
                    {
                        _selectedTower.Tower.IsSelected = false;
                    }

                    _selectedTower = value;
                    if (value != null)
                    {
                        value.Tower.IsSelected = true;
                    }

                    if (SelectedTowerChanged != null)
                    {
                        SelectedTowerChanged(_selectedTower);
                    }
                }
            }
        }

        public Vector2i MouseIndex
        {
            get { return _mouseIndex; }
            set { _mouseIndex = value; } // ..? Needs setter atm because it is set from PlayerController
        }

        #endregion

        public Player(World world)
        {
            _world = world;

            _world.WaveCompleted += OnWaveCompleted;
            _world.EnemyThrough += OnEnemyThrough;
            _world.EnemyKilled += OnEnemyKilled;

            this.Reset();
        }

        public bool BuyTower(TowerProxy towerProxy, Vector2i vector2i)
        {
            if (this.Coins >= towerProxy.Cost && _world.Map.AddTower(towerProxy, _mouseIndex))
            {
                towerProxy.InitializeToWorld(_world, _mouseIndex);
                this.Coins -= towerProxy.Cost;

                return true;
            }

            return false;
        }

        public void SellTower(TowerProxy towerProxy)
        {
            if (_world.Map.RemoveTower(towerProxy.Tower))
            {
                this.Coins += towerProxy.Cost;
                if (_selectedTower == towerProxy)
                {
                    this.SelectedTower = null;
                }
            }
        }

        public bool UpgradeTower(TowerProxy towerProxy, int upgradeIndex)
        {
            // Plaah.. Im gonna need a "TowerProxyProxy" to allow tower.Upgrade(..) to work...
            TowerProxy upgradedTowerProxy = towerProxy.UpgradePaths[upgradeIndex];
            if (this.Coins >= upgradedTowerProxy.Cost && _world.Map.RemoveTower(towerProxy.Tower))
            {
                this.Coins -= upgradedTowerProxy.Cost;
                if (_world.Map.AddTower(upgradedTowerProxy, towerProxy.Tower.WorldIndex))
                {
                    upgradedTowerProxy.InitializeToWorld(_world, towerProxy.Tower.WorldIndex);
                }
                else
                {
                    throw new Exception("");
                }

                if (_selectedTower == towerProxy)
                {
                    this.SelectedTower = upgradedTowerProxy;
                }

                return true;
            }

            return false;
        }

        public void Reset()
        {
           this.SelectedTower = null;
            this.CurrentlyAddingTower = null;

            this.Coins = Player.StartingCoins;
            _currentLives = Player.MaximumLives; 
        }

        private void OnWaveCompleted(int waveIndex, int completionCoins)
        {
            this.Coins += completionCoins;
        }

        private void OnEnemyThrough(Enemy enemy)
        {
            if (_currentLives > 0)
            {
                _currentLives--;
                if (_currentLives == 0)
                {
                    if (this.PlayerDied != null)
                    {
                        this.PlayerDied();
                    }
                }
            }
        }

        private void OnEnemyKilled(Enemy enemy, int coinsFromKill)
        {
            this.Coins += coinsFromKill;
        }
    }
}
