﻿
using Flai;
using Microsoft.Xna.Framework;
namespace SpaceBucks.Model
{
    public static class Tile
    {
        public const int Size = 40;
        public static Rectangle GetArea(Vector2i index)
        {
            return new Rectangle(index.X * Tile.Size, index.Y * Tile.Size, Tile.Size, Tile.Size);
        }
    }
}
