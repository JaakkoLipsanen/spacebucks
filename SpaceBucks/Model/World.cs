﻿using Flai;
using SpaceBucks.Model.Towers;

namespace SpaceBucks.Model
{
    public delegate void WaveCompletedHandler(int waveIndex, int completionCoins);
    public delegate void EnemyKilledHandler(Enemy enemy, int coinsFromKill);
    public delegate void EnemyThroughHandler(Enemy enemy);

    public class World
    {
        public event WaveCompletedHandler WaveCompleted;
        public event EnemyKilledHandler EnemyKilled;
        public event EnemyThroughHandler EnemyThrough;

        private readonly WorldMap _map;
        private readonly IWaveProvider _waveProvider;
        private Wave _currentWave;
        private int _currentWaveIndex;

        public WorldMap Map
        {
            get { return _map; }
        }

        public Wave CurrentWave
        {
            get { return _currentWave; }
        }

        public int CurrentWaveIndex
        {
            get { return _currentWaveIndex; }
        }
 
        public World(WorldMap map, IWaveProvider waveProvider)
        {
            _map = map;
            _waveProvider = waveProvider;

            this.Reset();
        }

        public void Update(UpdateContext updateContext, bool isPlayerAlive)
        {
            _currentWave.Update(updateContext);

            // MEH!!! This is pretty ugly
            if (isPlayerAlive)
            {
                foreach (Tower tower in _map.Towers)
                {
                    tower.Update(updateContext);
                }
            }

            if (_currentWave.IsCompleted)
            {
                this.OnWaveCompleted();     
            }
        }

        public void Reset()
        {
            _map.Reset();

            // If the wave is running, the signal that it has ended
            if (_currentWave != null && _currentWave.IsRunning)
            {
                if (WaveCompleted != null)
                {
                    WaveCompleted(_currentWaveIndex, 0);
                }
            }

            // Then reset the current wave to zero and move to the first wave
            _currentWaveIndex = 0;
            this.MoveToNextWave();
        }

        public void OnEnemyThrough(Enemy enemy)
        {
            if (EnemyThrough != null)
            {
                EnemyThrough(enemy);
            }
        }

        public void OnEnemyKilled(Enemy enemy)
        {
            if (EnemyKilled != null)
            {
                EnemyKilled(enemy, _currentWaveIndex * 5);
            }
        }

        private void OnWaveCompleted()
        {
            int waveIndex = _currentWaveIndex;
            this.MoveToNextWave();
            if (WaveCompleted != null)
            {
                WaveCompleted(waveIndex, waveIndex * 5); // previous wave
            }
        }

        private void MoveToNextWave()
        {
            _currentWaveIndex++;
            _currentWave = new Wave(_waveProvider.GetWave(_currentWaveIndex), this);
        }
    }
}
