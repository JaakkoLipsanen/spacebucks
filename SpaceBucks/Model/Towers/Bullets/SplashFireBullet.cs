﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceBucks.Model.Affectors;

namespace SpaceBucks.Model.Towers.Bullets
{
    public class SplashFireBullet : FireBulletBase
    {
        private const float OffsetMinMax = 24;
        private readonly Vector2 _offset = new Vector2(Global.Random.NextFloat(-OffsetMinMax, OffsetMinMax), Global.Random.NextFloat(-OffsetMinMax, OffsetMinMax));

        private float _totalTime = 0f;
        private float _alpha = 0f;

        public override bool CanBeRemoved
        {
            get { return this.HasHitTarget && _alpha <= 0f; }
        }

        public SplashFireBullet(float fireTime, float fireDps)
            : base(fireTime, fireDps)
        {
        }

        public override void Update(UpdateContext updateContext)
        {
            base.Update(updateContext);
            _totalTime += updateContext.DeltaSeconds;

            if (!this.HasHitTarget)
            {
                const float AlphaStartTime = 0.25f;
                _alpha = _totalTime > AlphaStartTime ? 1f : (_totalTime / AlphaStartTime);
            }
            else
            {
                _alpha -= updateContext.DeltaSeconds * 2f;
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            // DONT CALL BASE!
        }

        public override void DrawAdditive(GraphicsContext graphicsContext)
        {
            Color color = new Color(255, 92, 92);
            float rotation = FlaiMath.GetAngle(this.Target - this.Position);
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, color * 1f * (this.HasHitTarget ? _alpha : 1f), rotation, Tile.Size / 4f * 3f * (this.HasHitTarget ? 1f : _alpha));
        }

        protected override Vector2? GetOverrideTargetPosition()
        {
            if(_targetEnemy == null || !_targetEnemy.IsAlive && _targetEnemy.HasCompleted)
            {
                return null;
            }

            return _targetEnemy.Position + _offset;
        }

        protected override void OnTargetHit()
        {
            base.OnTargetHit();

            // Find all enemies within the range of the bullet (approximately) and apply a FireAffector to them
            foreach (Enemy enemy in base.World.CurrentWave.FindAllEnemiesWithinRange(this.Position, Tile.Size))
            {
                if (enemy != _targetEnemy && enemy.IsAlive)
                {
                    enemy.AddAffector(new FireAffector(_fireTime / 4f, _fireDps / 4f));
                }
            }
        }
    }
}
