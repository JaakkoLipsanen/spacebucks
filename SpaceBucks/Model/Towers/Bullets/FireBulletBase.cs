﻿using Flai.Graphics;
using Microsoft.Xna.Framework;
using SpaceBucks.Model.Affectors;

namespace SpaceBucks.Model.Towers.Bullets
{
    public abstract class FireBulletBase : RegularBullet
    {
        protected readonly float _fireTime;
        protected readonly float _fireDps;

        public FireBulletBase(float fireTime, float fireDps)
        {
            _fireTime = fireTime;
            _fireDps = fireDps;
            base.Color = new Color(255, 105, 65) * 0.5f;
        }

        protected override void OnTargetHit()
        {
            if (_targetEnemy.IsAlive)
            {
                _targetEnemy.AddAffector(new FireAffector(_fireTime, _fireDps));
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, base.Color, 0, Tile.Size / 2f);
        }
    }
}
