﻿using System;
using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Model.Towers.Bullets
{
    // TODO: Make a "Beam" class


    // and make the Bullet's render themselves!

    // scrap that last comment. make it abstract and make it easy to extend the bullet class
    // to every tower to have a different type. like CannonBullet, IceBullet etc.
    // also make some general bullet class that could be used in some towers and everytime
    // a new tower is implemented, that could be used for testing etc
    // also splash damage should be supported (though is splash a good property in a non-mazing td..? i mean 
    // most of the time enemies run separately...)

    // okay maybe this shouldn't be an abstract class..?

    // what the fuck am i gonna do? should all the towers have a list of bullets? should Bullet class
    // have rendering info? how the fuck am gonna expose the rendering info if the towers can "shoot"
    // other things than bullets, like beams. fuck!

    // Okay this will be horribly inefficient to make a Bullet class WHICH CAN EVEN BE INHERITED
    // but whatever. Maybe I could pool these or make them a struct later
    public abstract class Bullet
    {
        protected float _speed;
        protected float _damage;

        private World _world;
        private Vector2 _target;
        protected Enemy _targetEnemy;

        private bool _hasHitTarget = false;
        private float _speedMultiplier = 1f; // ??
        private Vector2 _position;

        protected bool HasHitTarget
        {
            get { return _hasHitTarget; }
        }

        public World World
        {
            get { return _world; }
            set
            {
                if (_world != null)
                {
                    throw new InvalidOperationException("World can't be re-setted");
                }

                _world = value;
            }
        }

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public float Damage
        {
            get { return _damage; }
            set { _damage = value; }
        }

        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public float SpeedMultiplier
        {
            get { return _speedMultiplier; }
            set { _speedMultiplier = value; }
        }

        public Enemy TargetEnemy
        {
            get { return _targetEnemy; }
            set
            {
                _targetEnemy = value;
                _target = value.Position;
            }
        }

        public Vector2 Target
        {
            get { return _target; }
        }

        public virtual bool CanBeRemoved
        {
            get { return _hasHitTarget; }
        }

        // Okay, while initializing all values in the constructor is nice idea and "clean", it makes creating new bullets a bit hard...
        protected Bullet()
        {
        }

        public virtual void Update(UpdateContext updateContext)
        {
            if (_hasHitTarget)
            {
                return;
            }

            Vector2? targetPosition = this.GetOverrideTargetPosition();
            if (targetPosition.HasValue)
            {
                _target = targetPosition.Value;
            }
            else if (_targetEnemy == null)
            {
                _target = _position;
            }
            else if (_targetEnemy.IsAlive)
            {
                _target = _targetEnemy.Position;
            }

            float distanceToTarget = Vector2.Distance(_position, _target);
            if (distanceToTarget < _speed * updateContext.DeltaSeconds)
            {
                _position = _target;
                this.OnTargetHitInner();             
            }
            else
            {
                _position = _position + Vector2.Normalize(_target - _position) * _speed * updateContext.DeltaSeconds;
            }
        }

        public virtual void Draw(GraphicsContext graphicsContext) { }
        public virtual void DrawAdditive(GraphicsContext graphicsContext) { }

        private void OnTargetHitInner()
        {
            if (_targetEnemy != null)
            {
                _targetEnemy.TakeDamage(_damage);
            }

            _hasHitTarget = true;
            this.OnTargetHit();
        }

        protected virtual void OnTargetHit() { }
        protected virtual Vector2? GetOverrideTargetPosition() { return null; }    }
}
