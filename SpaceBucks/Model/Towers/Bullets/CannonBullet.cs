﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using SpaceBucks.Model.Affectors;

namespace SpaceBucks.Model.Towers.Bullets
{
    public class CannonBullet : Bullet
    {
        private float _alpha = 1;
        private float _totalTime = 0f;
 
        public override bool CanBeRemoved
        {
            get { return _alpha <= 0; }
        }

        public override void Update(UpdateContext updateContext)
        {
            base.Update(updateContext);
            _totalTime += updateContext.DeltaSeconds;
            if (HasHitTarget)
            {
                _alpha -= updateContext.DeltaSeconds * 2;
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            const int N = 6;
            for (int i = 0; i < N; i++)
            {
                float rotation = FlaiMath.TwoPi / N * i;
                Vector2 rotationVector = FlaiMath.GetAngleVector(rotation);

                graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, Position + rotationVector * 8 / FlaiMath.Sqrt(_alpha), Color.SaddleBrown * 0.6f * _alpha, _totalTime * 4f, 16f);
            }
        }
    }
}
