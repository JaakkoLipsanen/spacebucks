﻿using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Model.Towers.Bullets
{
    public class RegularBullet : Bullet
    {
        private Color _color = Color.Red * 0.5f;
        private float _size = Tile.Size / 2f;

        public Color Color
        {
            get { return _color; }
            set { _color = value; }
        }

        public float Size
        {
            get { return _size; }
            set { _size = value; }
        }

        public RegularBullet()
        {
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, _color, 0, Tile.Size / 2f);
        }
    }
}
