﻿
using Flai;
using Flai.Graphics;
using XnaColor = Microsoft.Xna.Framework.Color;

namespace SpaceBucks.Model.Towers.Bullets
{
    public class NormalFreezeBullet : FreezeBulletBase
    {
        private float _totalTime = 0f;
        public NormalFreezeBullet(float freezeTime, float slowMultiplier, float freezeDps)
            : base(freezeTime, slowMultiplier, freezeDps)
        {
        }

        public override void Update(UpdateContext updateContext)
        {
            base.Update(updateContext);
            _totalTime += updateContext.DeltaSeconds;
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, XnaColor.RoyalBlue * 0.8f, _totalTime * 4f, Tile.Size / 3f);
        }
    }
}
