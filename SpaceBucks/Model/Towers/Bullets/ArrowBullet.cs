﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Model.Towers.Bullets
{
    public class ArrowBullet : RegularBullet
    {
        public ArrowBullet()
        {
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            float rotation = FlaiMath.GetAngle(base.Target - base.Position);
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, Color.DimGray, rotation, new Vector2(3, 12));
        }
    }
}
