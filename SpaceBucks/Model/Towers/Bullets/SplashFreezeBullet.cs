﻿using Flai;
using Flai.Graphics;
using Microsoft.Xna.Framework;
using SpaceBucks.Model.Affectors;

namespace SpaceBucks.Model.Towers.Bullets
{
    // And of course this can't inherit from SplashBullet and FreezeBulletBase... Make FreezeBullet
    public class SplashFreezeBullet : FreezeBulletBase
    {
        private const float OffsetMinMax = 32;
        private readonly Vector2 _offset = new Vector2(Global.Random.NextFloat(-OffsetMinMax, OffsetMinMax), Global.Random.NextFloat(-OffsetMinMax, OffsetMinMax));

        private float _totalTime = 0f;
        private float _alpha = 0f;

        public override bool CanBeRemoved
        {
            get { return this.HasHitTarget && _alpha <= 0f; }
        }

         public SplashFreezeBullet(float freezeTime, float slowMultiplier, float freezeDps)
            : base(freezeTime, slowMultiplier, freezeDps)
        {
        }

        public override void Update(UpdateContext updateContext)
        {
            base.Update(updateContext);
            _totalTime += updateContext.DeltaSeconds;

            if (!this.HasHitTarget)
            {
                const float AlphaStartTime = 0.15f;
                _alpha = _totalTime > AlphaStartTime ? 1f : (_totalTime / AlphaStartTime);
            }
            else
            {
                _alpha -= updateContext.DeltaSeconds * 1f;
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            Color color = new Color(212, 240, 255);
            float rotation = FlaiMath.GetAngle(this.Target - this.Position);
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, color * 0.35f * (this.HasHitTarget ? _alpha : 1f), rotation, Tile.Size / 4f * 3f * (this.HasHitTarget ? 1f : _alpha));
        }

        protected override Vector2? GetOverrideTargetPosition()
        {
            if(_targetEnemy == null || !_targetEnemy.IsAlive && _targetEnemy.HasCompleted)
            {
                return null;
            }

            return _targetEnemy.Position + _offset;
        }

        protected override void OnTargetHit()
        {
            base.OnTargetHit();

            // Find all enemies within the range of the bullet (approximately) and apply a "reduced" freeze effect to them
            foreach (Enemy enemy in base.World.CurrentWave.FindAllEnemiesWithinRange(this.Position, Tile.Size))
            {
                if (enemy != _targetEnemy && enemy.IsAlive)
                {
                    enemy.AddAffector(new FreezeAffector(_freezeTime / 4f, (2 + _freezeSlowMultiplier) / 5f, _freezeDps / 4f));
                }
            }
        }
    }
}
