﻿using Flai.Graphics;
using SpaceBucks.Model.Affectors;
using XnaColor = Microsoft.Xna.Framework.Color;

namespace SpaceBucks.Model.Towers.Bullets
{
    public abstract class FreezeBulletBase : RegularBullet
    {
        protected readonly float _freezeTime;
        protected readonly float _freezeSlowMultiplier;
        protected readonly float _freezeDps;

        public FreezeBulletBase(float freezeTime, float slowMultiplier, float freezeDps)
        {
            _freezeTime = freezeTime;
            _freezeSlowMultiplier = slowMultiplier;
            _freezeDps = freezeDps;
            base.Color = new XnaColor(65, 105, 255) * 0.5f;
        }

        protected override void OnTargetHit()
        {
            if(_targetEnemy.IsAlive)
            {
                _targetEnemy.AddAffector(new FreezeAffector(_freezeTime, _freezeSlowMultiplier, _freezeDps));
            }
        }

        public override void Draw(GraphicsContext graphicsContext)
        {
            graphicsContext.SpriteBatch.DrawCentered(graphicsContext.BlankTexture, base.Position, base.Color, 0, Tile.Size / 2f);
        }
    }
}
