﻿using Flai;
using Microsoft.Xna.Framework;

namespace SpaceBucks.Model.Towers
{
    // Should it be possible to upgrade towers without creating a new Tower object...?
    public abstract class Tower
    {
        protected readonly World _world;
        protected readonly Vector2i _worldIndex;

        public bool IsSelected { get; set; }
      
        // Should this be WorldArea etc.? that way towers could be 2x2 etc big
        public Vector2i WorldIndex 
        {
            get { return _worldIndex; }
        }

        public RectangleF Area
        {
            get { return new RectangleF(_worldIndex.X * Tile.Size, _worldIndex.Y * Tile.Size, Tile.Size, Tile.Size); }
        }

        protected Vector2 CenterPosition
        {
            get { return new Vector2(_worldIndex.X + 0.5f, _worldIndex.Y + 0.5f) * Tile.Size; }
        }

        // TODO: Move world and world index away from the constructor? So that
        // they would be initialized in WorldMap.AddTower or something like that and wouldn't be needed
        // on every time a tower is created
        public Tower(World world, Vector2i worldIndex)
        {
            _world = world;
            _worldIndex = worldIndex;
        }

        public virtual void Update(UpdateContext updateContext)
        {
        }
    }
}
