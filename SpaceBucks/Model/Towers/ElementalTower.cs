﻿
using Flai;
using SpaceBucks.Model.Towers.Bullets;
namespace SpaceBucks.Model.Towers
{
    // Should this inherit RegularTower..? Maybe..?
    // Ice tower -> Regular Tower makes sense. Fire Tower -> makes sense maybe? Poison -> yeaah...?
    public abstract class ElementalTower : RegularTower<RegularBullet>
    {
        public ElementalTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }
    }
}
