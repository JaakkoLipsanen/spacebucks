﻿using System;
using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public class FreezeTowerProxy : RegularTowerProxy
    {
        protected override float AttackSpeed
        {
            get { return 1.25f; }
        }

        protected override float AttackRange
        {
            get { return 150; }
        }

        protected override Range DamageRange
        {
            get { return new Range(8, 16); }
        }

        protected override float BulletSpeed
        {
            get { return 400f; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Ice; }
        }

        public override int Cost
        {
            get { return 20; }
        }

        public override string Name
        {
            get { return "Freeze Tower"; }
        }

        public override TowerProxy[] UpgradePaths
        {
            get 
            {
                // TODO: Add SplashFreezeTowerProxy
                return new TowerProxy[] { new MultiFreezeTowerProxy(1), new SplashFreezeTowerProxy(1) };
            }
        }

        public override TowerProxy Clone()
        {
            return new FreezeTowerProxy();
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new FreezeTower(world, worldIndex, this.AttackSpeed, this.AttackRange, this.DamageRange, this.BulletSpeed);
        }
    }
}
