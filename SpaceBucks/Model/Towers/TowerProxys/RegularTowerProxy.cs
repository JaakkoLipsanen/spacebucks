﻿using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public abstract class RegularTowerProxy : TowerProxy
    {
        protected abstract float AttackSpeed { get; }
        protected abstract float AttackRange { get; }
        protected abstract Range DamageRange { get; }
        protected abstract float BulletSpeed { get; }

        public override int DPS
        {
            get { return (int)(this.DamageRange.Average / this.AttackSpeed); }
        }
    }
}
