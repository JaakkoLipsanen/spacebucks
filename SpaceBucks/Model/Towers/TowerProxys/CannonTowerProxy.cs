﻿using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public class CannonTowerProxy : RegularTowerProxy
    {
       private const int CannonTowerBaseCost = 20;
        private readonly int _level;

        public override int Cost
        {
            get { return _level == 1 ? CannonTowerBaseCost : (CannonTowerBaseCost * (_level * _level * _level / 3)); }
        }

        public override string Name
        {
            get { return "Cannon Tower " + _level; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Cannon; }
        }

        public override TowerProxy[] UpgradePaths
        {
            get
            {
                if (_level == 10)
                {
                    return new TowerProxy[0];
                }

                return new TowerProxy[] { new CannonTowerProxy(_level + 1), new ArrowTowerProxy(_level), new ElementalTowerProxy(_level) }; 
            }
        }

        protected override float AttackRange
        {
            get {  return 100 + _level * 15;}
        }

        protected override float AttackSpeed
        {
            get { return 3.5f / (1 + _level / 4f); }
        }

        protected override Range DamageRange
        {
            get { return new Range(20 * (_level * _level), 40 * (_level * _level) + 12 * _level); }
        }

        protected override float BulletSpeed
        {
            get { return 250f + _level * 20; }
        }

        public CannonTowerProxy(int level)
        {
            _level = level;
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new CannonTower(world, worldIndex,
                this.AttackSpeed,
                this.AttackRange,
                this.DamageRange,
                this.BulletSpeed);
        }


        public override TowerProxy Clone()
        {
            return new CannonTowerProxy(_level);
        }
    }
}
