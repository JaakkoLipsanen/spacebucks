﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public class SplashFreezeTowerProxy : RegularTowerProxy
    {
        private readonly int _level = 1;

        private float FreezeTime
        {
            get { return 3f + _level * 0.15f; }
        }

        private float FreezeSlowMultiplier
        {
            get { return 0.5f /  (1 + _level / 4f); }
        }

        private float FreezeDPS
        {
            get { return 2 * _level / 2f; }
        }

        protected override float AttackSpeed
        {
            get { return 0.025f / (1 + _level / 2f); }
        }

        protected override float AttackRange
        {
            get { return 120 + _level * 15; }
        }

        protected override Range DamageRange
        {
            get { return new Range(_level * _level / 8f,  _level * _level / 6f); }
        }

        protected override float BulletSpeed
        {
            get { return 500f + _level * 35; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Ice; }
        }

        public override int Cost
        {
            get
            { 
                const int BaseCost = 100;
                return BaseCost * _level;
            }
        }

        public override string Name
        {
            get { return "Splash Freeze Tower " + _level; }
        }

        public SplashFreezeTowerProxy(int level)
        {
            _level = level;
        }

        public override TowerProxy[] UpgradePaths
        {
            get
            {
                return new TowerProxy[]
                {
                    new SplashFreezeTowerProxy(_level + 1)
                };
            }
        }

        public override TowerProxy Clone()
        {
            return new SplashFreezeTowerProxy(_level);
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new SplashFreezeTower(world, worldIndex, this.AttackSpeed, this.AttackRange, this.DamageRange, this.BulletSpeed, this.FreezeTime, this.FreezeSlowMultiplier, this.FreezeDPS);
        }
    }
}
