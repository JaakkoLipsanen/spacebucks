﻿
using Flai;
namespace SpaceBucks.Model.Towers.TowerProxys
{
    public abstract class TowerProxy
    {
        private Tower _tower;
        public Tower Tower
        {
            get { return _tower; }
        }

        public abstract TowerType TowerType { get; }
        public abstract int Cost { get; }
        public abstract int DPS { get; }
        public abstract string Name { get; }
        public abstract TowerProxy[] UpgradePaths { get; }
        public abstract TowerProxy Clone();

        protected abstract Tower CreateTower(World world, Vector2i worldIndex);
        public void InitializeToWorld(World world, Vector2i worldIndex)
        {
            if(_tower != null)
            {
                FlaiLogger.Log("The tower is already created!");
            }

            _tower = this.CreateTower(world, worldIndex);
        }
    }
}
