﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public class SplashFireTowerProxy : RegularTowerProxy
    {
        private readonly int _level = 1;

        private float FireTime
        {
            get { return 1f + _level * 0.15f; }
        }

        private float FireDPS
        {
            get { return 16 * _level * _level / 4f * 3f; }
        }

        protected override float AttackSpeed
        {
            get { return 0.015f / (1 + _level / 2f); }
        }

        protected override float AttackRange
        {
            get { return 140 + _level * 20; }
        }

        protected override Range DamageRange
        {
            get { return new Range(_level * _level / 8f, _level * _level / 6f); }
        }

        protected override float BulletSpeed
        {
            get { return 500f + _level * 35; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Fire; }
        }

        public override int Cost
        {
            get
            {
                const int BaseCost = 100;
                return BaseCost * _level;
            }
        }

        public override string Name
        {
            get { return "Splash Fire Tower " + _level; }
        }

        public SplashFireTowerProxy(int level)
        {
            _level = level;
        }

        public override TowerProxy[] UpgradePaths
        {
            get
            {
                return new TowerProxy[]
                {
                    new SplashFireTowerProxy(_level + 1)
                };
            }
        }

        public override TowerProxy Clone()
        {
            return new SplashFireTowerProxy(_level);
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new SplashFireTower(world, worldIndex, this.AttackSpeed, this.AttackRange, this.DamageRange, this.BulletSpeed, this.FireTime,  this.FireDPS);
        }
    }
}
