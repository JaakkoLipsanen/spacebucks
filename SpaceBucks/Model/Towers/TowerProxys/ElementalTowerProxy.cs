﻿using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    // Okay, this was a level based before, but that is removed.
    // However I can't be to actually refactor this class so I will keep the "functionality"
    public class ElementalTowerProxy : RegularTowerProxy
    {
        private const int ElementalTowerBaseCost = 15;
        private readonly int _level;

        public override int Cost
        {
            get { return _level == 1 ? ElementalTowerBaseCost : (ElementalTowerBaseCost * (_level * _level * _level / 3)); }
        }

        public override string Name
        {
            get { return "Elemental Tower " + _level; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Elemental; }
        }

        public override TowerProxy[] UpgradePaths
        {
            get
            {
                if (_level == 10)
                {
                    return new TowerProxy[0];
                }

                return new TowerProxy[] { new FreezeTowerProxy(), new SplashFireTowerProxy(1) };
            }
        }

        protected override float AttackRange
        {
            get { return 150 + _level * 20; }
        }

        protected override float AttackSpeed
        {
            get { return 1.25f / (1 + _level / 4f); }
        }

        protected override Range DamageRange
        {
            get { return new Range(5 * (_level * _level), 12 * (_level * _level) + 10 * _level); }
        }

        protected override float BulletSpeed
        {
            get { return 400f + _level * 20; }
        }

        public ElementalTowerProxy(int level)
        {
            _level = level;
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new BaseElementalTower(world, worldIndex,
                this.AttackSpeed,
                this.AttackRange,
                this.DamageRange,
                this.BulletSpeed);
        }


        public override TowerProxy Clone()
        {
            return new ElementalTowerProxy(_level);
        }
    }
}
