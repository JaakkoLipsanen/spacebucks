﻿using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public class ArrowTowerProxy : RegularTowerProxy
    {
        private const int ArrowTowerBaseCost = 15;
        private readonly int _level;

        public override int Cost
        {
            get { return _level == 1 ? ArrowTowerBaseCost : (ArrowTowerBaseCost * (_level * _level * _level / 3)); }
        }

        public override string Name
        {
            get { return "Arrow Tower " + _level; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Arrow; }
        }

        public override TowerProxy[] UpgradePaths
        {
            get
            {
                if (_level == 10)
                {
                    return new TowerProxy[0];
                }

                return new TowerProxy[] { new ArrowTowerProxy(_level + 1) }; 
            }
        }

        protected override float AttackRange
        {
            get {  return 200 + _level * 25;}
        }

        protected override float AttackSpeed
        {
            get { return 1.25f / (1 + _level / 4f); }
        }

        protected override Range DamageRange
        {
            get { return new Range(5 * (_level * _level), 12 * (_level * _level) + 10 * _level); }
        }

        protected override float BulletSpeed
        {
            get { return 400f + _level * 20; }
        }
        
        public ArrowTowerProxy(int level)
        {
            _level = level;
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new ArrowTower(world, worldIndex,
                this.AttackSpeed,
                this.AttackRange,
                this.DamageRange,
                this.BulletSpeed);
        }

        public override TowerProxy Clone()
        {
            return new ArrowTowerProxy(_level);
        }
    }
}
