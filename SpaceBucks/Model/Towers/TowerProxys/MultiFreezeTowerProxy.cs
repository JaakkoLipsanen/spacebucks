﻿using Flai;

namespace SpaceBucks.Model.Towers.TowerProxys
{
    public class MultiFreezeTowerProxy : RegularTowerProxy
    {
        private readonly int _level = 1;

        private float FreezeTime
        {
            get { return 1.25f + _level * 0.05f; }
        }

        private float FreezeSlowMultiplier
        {
            get { return 0.9f /  (1 + _level / 4f); }
        }

        private float FreezeDPS
        {
            get { return 3 * _level; }
        }

        protected override float AttackSpeed
        {
            get { return 1.25f / (1 + _level / 2f); }
        }

        protected override float AttackRange
        {
            get { return 200 + _level * 30; }
        }

        protected override Range DamageRange
        {
            get { return new Range(0, 0); }
        }

        protected override float BulletSpeed
        {
            get { return 500f + _level * 35; }
        }

        public override TowerType TowerType
        {
            get { return TowerType.Ice; }
        }

        public override int Cost
        {
            get
            { 
                const int BaseCost = 100;
                return BaseCost * _level;
            }
        }

        public override string Name
        {
            get { return "Multi Freeze Tower " + _level; }
        }

        public MultiFreezeTowerProxy(int level)
        {
            _level = level;
        }

        public override TowerProxy[] UpgradePaths
        {
            get
            {
                return new TowerProxy[]
                {
                    new MultiFreezeTowerProxy(_level + 1)
                };
            }
        }

        public override TowerProxy Clone()
        {
            return new MultiFreezeTowerProxy(_level);
        }

        protected override Tower CreateTower(World world, Vector2i worldIndex)
        {
            return new MultiFreezeTower(world, worldIndex, this.AttackSpeed, this.AttackRange, this.DamageRange, this.BulletSpeed, this.FreezeTime, this.FreezeSlowMultiplier, this.FreezeDPS);
        }
    }
}
