﻿
using SpaceBucks.Model.Towers.TowerProxys;
namespace SpaceBucks.Model.Towers
{
    public enum TowerType
    {
        Arrow,
        Cannon,
        Elemental,
        Fire,
        Ice,
    }

    public static class TowerTypeExtensions
    {
        public static char ToChar(this TowerType towerType)
        {
            switch (towerType)
            {
                case TowerType.Arrow:
                    return 'A';

                case TowerType.Cannon:
                    return 'C';

                case TowerType.Elemental:
                    return 'E';

                case TowerType.Fire:
                    return 'F';

                case TowerType.Ice:
                    return 'I';

                default:
                    return 'U';
            }
        }

        public static TowerProxy CreateTowerProxy(this TowerType towerType)
        {
            switch (towerType)
            {
                case TowerType.Arrow:
                    return new ArrowTowerProxy(1);

                case TowerType.Cannon:
                    return new CannonTowerProxy(1);

                case TowerType.Elemental:
                    return new ElementalTowerProxy(1);

                // Fire and Ice can only be upgraded from Elemental

                default:
                    return null;
            }
        }
    }
}
