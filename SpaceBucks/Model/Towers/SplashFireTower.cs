﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    public class SplashFireTower : RegularTower
    {     
        private readonly float _fireTime;
        private readonly float _dpsAttackDamage;

        public SplashFireTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed, float fireTime, float dpsAttackDamage)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
            _fireTime = fireTime;
            _dpsAttackDamage = dpsAttackDamage; 
        }

        protected override Bullet CreateBullet()
        {
            return new SplashFireBullet(_fireTime, _dpsAttackDamage);
        }
    }
}
