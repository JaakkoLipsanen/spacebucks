﻿using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    // Tower which hits all the enemies in the range
    public class MultiTower<T> : RegularTower
        where T : Bullet, new()
    {
        public MultiTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }

        // Fires all the bullets at the same time
        protected override void OnReadyToShoot()
        {
            foreach (Enemy enemy in _world.CurrentWave.FindAllEnemiesWithinRange(base.CenterPosition, _attackRange))
            {
                Bullet bullet = this.CreateBullet();
                bullet.TargetEnemy = enemy;

                this.FireBullet(bullet);
            }

            _timeSinceLastAttack = 0;
        }

        protected override Bullet CreateBullet()
        {
            return new T();
        }
    }

    // Tower which hits all the enemies in the range
    public abstract class MultiTower : RegularTower
    {
        public MultiTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range  damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }

        // Fires all the bullets at the same time
        protected override void OnReadyToShoot()
        {
            foreach (Enemy enemy in _world.CurrentWave.FindAllEnemiesWithinRange(base.CenterPosition, _attackRange))
            {
                Bullet bullet = this.CreateBullet();
                bullet.TargetEnemy = enemy;

                this.FireBullet(bullet);
            }

            _timeSinceLastAttack = 0;
        }
    }
}
