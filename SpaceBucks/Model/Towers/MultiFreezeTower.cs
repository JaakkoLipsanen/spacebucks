﻿using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    public class MultiFreezeTower : MultiTower
    {
        private readonly float _freezeTime;
        private readonly float _freezeSlowMultiplier;
        private readonly float _dpsAttackDamage;

        public MultiFreezeTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed, float freezeTime, float slowMultiplier,  float dpsAttackDamage)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
            _freezeTime = freezeTime;
            _freezeSlowMultiplier = slowMultiplier;
            _dpsAttackDamage = dpsAttackDamage; 
        }

        protected override Bullet CreateBullet()
        {
            return new NormalFreezeBullet(_freezeTime, _freezeSlowMultiplier, _dpsAttackDamage);
        }
    }
}
