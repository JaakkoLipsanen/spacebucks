﻿using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    public class BaseElementalTower : RegularTower<RegularBullet>
    {
        public BaseElementalTower(World world, Vector2i worldIndex,float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }
    }
}
