﻿using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    // FreezeTower will be only the first level.
    // After Level1, there will be either "SplashFreezeTower" which fires bullets all the time to multiple enemies (with splash maybe :P ?) with high slow-rate
    // Or, multi freeze tower which hits multiple enemies with low slow-rate
    public class FreezeTower : RegularTower
    {
        public FreezeTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }

        protected override Bullet CreateBullet()
        {
            return new NormalFreezeBullet(2, 0.85f, 4);
        }
    }
}
