﻿using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    // meh, this class isn't actually needed but it might be clearer than using just "RegularTower<ArrowBullet>"
    public class ArrowTower : RegularTower<ArrowBullet> 
    {
        public ArrowTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }
    }
}
