﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Flai;
using Microsoft.Xna.Framework;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    public class RegularTower<T> : RegularTower
        where T : Bullet, new()
    {
        public RegularTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }

        protected override Bullet CreateBullet()
        {
            return new T();
        }
    }


    // Okay I'm gonna make

    // Super awful name. Basically a tower, which shoots on a regular basis
    // and has attack speed, range etc
    // For example arrow tower, cannon tower etc.
    // NOT beam tower etc
    public abstract class RegularTower : Tower
    {
        private Enemy _targetEnemy;
        protected readonly float _attackSpeed;
        protected readonly float _attackRange;
        protected readonly Range _damageRange;
        protected readonly float _bulletSpeed;

        private readonly List<Bullet> _innerBullets = new List<Bullet>();
        protected readonly ReadOnlyCollection<Bullet> _bullets;

        protected float _timeSinceLastAttack = 0;

        public ReadOnlyCollection<Bullet> Bullets
        {
            get { return _bullets; }
        }

        public float AttackRange
        {
            get { return _attackRange; }
        }

        public RegularTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex)
        {
            _attackSpeed = attackSpeed;
            _attackRange = attackRange;
            _damageRange = damageRange;
            _bulletSpeed = bulletSpeed;

            _bullets = new ReadOnlyCollection<Bullet>(_innerBullets);
        }

        public override void Update(UpdateContext updateContext)
        {
            base.Update(updateContext);

            foreach (Bullet bullet in _innerBullets)
            {
                bullet.Update(updateContext);
            }
            _innerBullets.RemoveAll(bullet => bullet.CanBeRemoved);

            _timeSinceLastAttack += updateContext.DeltaSeconds;
            if (_timeSinceLastAttack >= _attackSpeed)
            {
                this.OnReadyToShoot();
            }
        }

        protected virtual void OnReadyToShoot()
        {
            if (_targetEnemy == null || !_targetEnemy.IsAlive || _targetEnemy.HasCompleted || Vector2.Distance(base.CenterPosition, _targetEnemy.Position) > _attackRange)
            {
                 _targetEnemy = _world.CurrentWave.FindNearestEnemy(base.CenterPosition, _attackRange);
            }

            if (_targetEnemy != null)
            {
                Bullet bullet = this.CreateBullet();
                if (bullet != null)
                {
                    bullet.TargetEnemy = _targetEnemy;
                    this.FireBullet(bullet);
                }
                else
                {
                    _targetEnemy.TakeDamage(this.GetRandomDamage());
                }

                _timeSinceLastAttack = 0;
            }
        }

        protected float GetRandomDamage()
        {
            return Global.Random.NextFloat(_damageRange.Min, _damageRange.Max);
        }

        protected abstract Bullet CreateBullet();
        protected void FireBullet(Bullet bullet)
        {
            float damage = this.GetRandomDamage();
            bullet.Damage = damage;
            bullet.Speed = _bulletSpeed;
            bullet.Position = base.CenterPosition;
            bullet.World = _world;

            _innerBullets.Add(bullet);
        }
    }
}
