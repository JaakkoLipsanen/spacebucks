﻿using Flai;
using SpaceBucks.Model.Towers.Bullets;

namespace SpaceBucks.Model.Towers
{
    public class CannonTower : RegularTower<CannonBullet>
    {
        public CannonTower(World world, Vector2i worldIndex, float attackSpeed, float attackRange, Range damageRange, float bulletSpeed)
            : base(world, worldIndex, attackSpeed, attackRange, damageRange, bulletSpeed)
        {
        }
    }
}
