﻿
using Microsoft.Xna.Framework;
using System;
using Flai.Extensions;
using Flai;

namespace SpaceBucks.Model
{
    public static class WorldGenerator
    {
        public static World Generate()
        {
            WorldMap worldMap = WorldGenerator.GenerateMap();
            WaveProvider waveProvider = new WaveProvider(worldMap);
            return new World(worldMap, waveProvider);
        }

        private static WorldMap GenerateMap()
        {
            const int Width = 32;
            const int Height = 24;

            #region Is Solid Map Initialization
            bool[] isSolidMap = new bool[Width * Height]
            {
                true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                true, true, true, true, true, false, false, false, false, false, false, true, true, true, true, false, false, true, true, true, true, false, false, false, false, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, false, false, false, false, true, true, true, true, false, false, true, true, true, true, false, false, false, false, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, true, true, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, true, true, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, false, false, false, false, true, true, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, false, false, false, false, true, true, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, false, false, false, false, true, true, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, false, false, false, false, true, true, false, false, false, false, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, false, false, false, false, false, false, true, true, true, true, true, 
                true, true, true, true, true, false, false, false, false, false, false, true, true, false, false, true, true, false, false, true, true, false, false, false, false, false, false, true, true, true, true, true, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, true, true, false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
                false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, 
                true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, 
            };
            #endregion

            bool[] isBuildableMap = new bool[isSolidMap.Length];
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    int i = x + y * Width;
                    isBuildableMap[i] = true;

                    // The tower list UI
                    if (y < 4 && x < 3)
                    {
                        isBuildableMap[i] = false;
                    }
                    // The tower info UI
                    else if (x > Width - 4 && y < 10)
                    {
                        isBuildableMap[i] = false;
                    }
                    // Bottom
                    else if (y > Height - 3)
                    {
                        isBuildableMap[i] = false;
                    }
                }
            }

            #region Enemy Path
            Vector2[] enemyPath = new Vector2[]
            {
                new Vector2(-2, 21),
                new Vector2(14, 21),
                new Vector2(14, 12),
                new Vector2(10, 12),
                new Vector2(10, 15),
                new Vector2(6, 15),
                new Vector2(6, 3),
                new Vector2(10, 3),
                new Vector2(10, 8),
                new Vector2(14, 8),
                new Vector2(14, 5),
                new Vector2(16, 5),
                new Vector2(16, -1),
            };
            #endregion

            return new WorldMap(Width, Height, isSolidMap, isBuildableMap, enemyPath, AlternateAxis.X);
        }

        #region Secret code to generate code from level

        //var sb = new System.Text.StringBuilder();
        //sb.AppendLine("bool[] isSolidMap = new bool[Width * Height]");
        //sb.AppendLine("{");
        //for (int y = 0; y < _map.Height; y++)
        //{
        //    sb.Append('\t');
        //    for (int x = 0; x < _map.Width; x++)
        //    {
        //        sb.Append(_map.IsFree(x, y) ? "true, " : "false, ");
        //    }

        //    sb.AppendLine();
        //}

        //sb.AppendLine("};");

        //string str = sb.ToString();

        #endregion

        #region Wave Provider

        private class WaveProvider : IWaveProvider
        {
            private readonly WorldMap _worldMap;
            public WaveProvider(WorldMap worldMap)
            {
                _worldMap = worldMap;
            }

            public IWaveData GetWave(int waveIndex)
            {
                const int EnemyCount = 20;
                return new WaveData((index) =>
                {
                    const float BaseSpeed = 75f;
                    return new Enemy(BaseSpeed + 10 * waveIndex, 30 + FlaiMath.Pow(waveIndex / 2f, 5), _worldMap.GetPath(index));
                }, EnemyCount);
            }

            private class WaveData : IWaveData
            {
                private readonly Func<int, Enemy> _createEnemyAction;
                private readonly int _enemyCount;
                private readonly float _timeBetweenEnemies;

                public int EnemyCount
                {
                    get { return _enemyCount; }
                }

                public float TimeBetweenEnemySpawns
                {
                    get { return _timeBetweenEnemies; }
                }

                public WaveData(Func<int, Enemy> createEnemy, int enemyCount, float timeBetweenEnemies = 1)
                {
                    _createEnemyAction = createEnemy;
                    _enemyCount = enemyCount;
                    _timeBetweenEnemies = timeBetweenEnemies;
                }

                public Enemy CreateEnemy(int index)
                {
                    return _createEnemyAction(index);
                }
            }
        }

        #endregion
    }
}
