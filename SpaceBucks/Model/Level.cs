﻿using Flai;

namespace SpaceBucks.Model
{
    public class Level
    {
        private readonly World _world;
        private readonly Player _player;

        public World World
        {
            get { return _world; }
        }

        public Player Player
        {
            get { return _player; }
        }

        public Level(World world)
        {
            _world = world;
            _player = new Player(_world);
        }

        public void Update(UpdateContext updateContext)
        {
            _world.Update(updateContext, _player.IsAlive);
        }

        public void Reset()
        {
            _player.Reset();
            _world.Reset();
        }

        public static Level Generate()
        {
            return new Level(WorldGenerator.Generate());
        }
    }
}
