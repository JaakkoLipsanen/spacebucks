﻿using System.Collections.Generic;
using Flai;
using Microsoft.Xna.Framework;
using SpaceBucks.Model.Affectors;

namespace SpaceBucks.Model
{
    // Implement "affectors". Like FreezeAffector, PoisonAffector, FlameAffector, StunAffector etc etc
    // This is in prorgress atm

    // Maybe implement "special abilities", like revival or "shield" that will be activated at some point or super-speed etc?
    public class Enemy
    {
        private readonly float _speed;
        private readonly float _totalHealth;
        private readonly IEnumerator<Vector2> _path;
        private readonly AffectorManager _affectorManager = new AffectorManager();

        private float _currentHealth;
        private Vector2 _position;
        private bool _hasCompleted = false;

        protected float Speed
        {
            get { return _affectorManager.ProcessSpeed(_speed); }
        }

        public bool IsAlive
        {
            get { return _currentHealth > 0; }
        }

        public bool HasCompleted
        {
            get { return _hasCompleted; }
        }

        public Vector2 Position
        {
            get { return _position; }
        }

        public float MaximumHealth
        {
            get { return _totalHealth; }
        }

        public float CurrentHealth
        {
            get { return _currentHealth; }
        }

        public Color Color
        {
            get { return _affectorManager.EnemyTint; }
        }

        public Enemy(float speed, float health, IEnumerator<Vector2> path)
        {
            _speed = speed;
            _totalHealth = health;
            _path = path;
      
            _currentHealth = health;

            // Set position to the first path checkpoint
            _path.MoveNext();
            _position = _path.Current;
            _path.MoveNext();
        }

        public void Update(UpdateContext updateContext)
        {
            float remainingDistance = this.Speed * updateContext.DeltaSeconds;
            while (true)
            {
                Vector2 nextPosition = _path.Current;
                float distanceToNextCheckpoint = Vector2.Distance(_position, nextPosition);

                if (distanceToNextCheckpoint < remainingDistance)
                {
                    _position = nextPosition;
                    remainingDistance -= distanceToNextCheckpoint;

                    if (!_path.MoveNext())
                    {
                        _hasCompleted = true;
                        break;
                    }
                }
                else
                {
                    _position = Vector2.Lerp(_position, nextPosition, remainingDistance / distanceToNextCheckpoint);
                    break;
                }
            }

            _affectorManager.Update(updateContext);
        }

        public void TakeDamage(float damage)
        {
            if (this.IsAlive)
            {
                _currentHealth -= _affectorManager.ProcessDamageInput(damage);
            }
        }

        public void AddAffector(Affector affector)
        {
            affector.Target = this;
            _affectorManager.AddAffector(affector);
        }
    }
}
