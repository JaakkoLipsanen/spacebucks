﻿
namespace SpaceBucks.Model
{
    public interface IWaveData
    {
        int EnemyCount { get; }
        float TimeBetweenEnemySpawns { get; }

        Enemy CreateEnemy(int index);
    }
}
