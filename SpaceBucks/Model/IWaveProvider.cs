﻿
namespace SpaceBucks.Model
{
    public interface IWaveProvider
    {
        IWaveData GetWave(int waveIndex);
    }
}
